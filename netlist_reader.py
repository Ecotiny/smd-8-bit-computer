# reads KiCAD Netlist to gernerate things

def get_components(filename):
    with open(filename, "r+") as f:
        lines = f.readlines()
        
        components = []
        
        current = {}
        current_ref = ""
        current_val = ""
        current_footprint = ""
        
        LS_to_HC = True
        
        first_one = True
        
        for line in lines:
            if "(comp " in line:
                if not first_one:
                    components.append(current)
                    current = {}
                strline = line.strip()
                reference = strline[11:][:-1].strip()
                current["reference"] = reference
                first_one = False
            elif "(value " in line:
                strline = line.strip()
                value = strline[7:][:-1]
                if LS_to_HC and "74LS" in value:
                    value = "74HC" + value[4:]
                current["value"] = value
            elif "(footprint " in line:
                strline = line.strip()
                footprint = strline[11:][:-1]
                foots = footprint.split(":")
                footname = foots[1]
                current["footprint"] = footname

    return components

def count_array(arr):
    count = {}
    for item in arr:
        if item not in count.keys():
            count[item] = 1
        else:
            count[item] = count[item] + 1
            
    return count

def print_dict(dictionary):
    for k in sorted(list(dictionary.keys())):
        print("{}: {}".format(k,dictionary[k]))

def print_component(comp):
    print("========\nCOMPONENT {}".format(comp["reference"]))
    print("Value: {}".format(comp["value"]))
    print("Footprint: {}".format(comp["footprint"]))
    
counting = True 
detailing = False 

components = get_components("transistor-computer/nand.net")

# get count
capacitors = []
diodes = []
resistors = []
ics = []
fuses = []
connectors = []
switches = []
transistors = []

for component in components:
    ref = component["reference"]
    val = component["value"]
    
    if "D" in ref: # it is a diode
        diodes.append(val)
    elif "C" in ref:
        capacitors.append(val)
    elif "R" in ref:
        resistors.append(val)
    elif "U" in ref:
        ics.append(val)
    elif "F" in ref:
        fuses.append(val)
    elif "J" in ref:
        connectors.append(val)
    elif "SW" in ref:
        switches.append(val)
    elif "Q" in ref:
        transistors.append(val)
    else:
        print("UNHANDLED COMPONENT\n{}\n{}".format(ref, val))

c_count  = count_array(capacitors)        
d_count  = count_array(diodes)
r_count  = count_array(resistors)
u_count  = count_array(ics)
f_count  = count_array(fuses)
j_count  = count_array(connectors)
sw_count = count_array(switches)
q_count  = count_array(transistors)

if counting:
    print("========\nCapacitors\n========")
    print_dict(c_count)
    print("========\nDiodes\n========")
    print_dict(d_count)
    print("========\nResistors\n========")
    print_dict(r_count)
    print("========\nICs\n========")
    print_dict(u_count)
    print("========\nFuses\n========")
    print_dict(f_count)
    print("========\nConnectors\n========")
    print_dict(j_count)
    print("========\nSwitches\n========")
    print_dict(sw_count)
    print("========\nTransistors\n========")
    print_dict(q_count)

## DO THE DETAILING
if detailing:
    for component in components:
        print_component(component)
