module nonblocking(input clk, input reset);
    reg [2:0] a,b,c;
    
    always @(posedge clk)
    begin
        if (reset == 1)
        begin
            a <= 0;
            b <= 0;
            c <= 0;
        end
        else if (reset == 0)
        begin
            a <= a + 1;
            b <= b + 1;
            c <= c + 1;
        end
        $display("%d %d %d", a, b, c);
    end
endmodule

module testbench();
    reg clk;
    reg [10:0] count;
