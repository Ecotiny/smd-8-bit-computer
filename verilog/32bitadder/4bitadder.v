`include "1bitadder.v"

module fourbitadder(cIn, a, b, sum, cOut);
    input cIn;
    input [3:0] a;
    input [3:0] b;
    output [3:0] sum;
    output cOut;
    
    wire c1,c2,c3;
    
    onebitadder a1(.cIn(cIn), .a(a[0]), .b(b[0]), .sum(sum[0]), .cOut(c1)   );
    onebitadder a2(.cIn(c1),  .a(a[1]), .b(b[1]), .sum(sum[1]), .cOut(c2)   );
    onebitadder a3(.cIn(c2),  .a(a[2]), .b(b[2]), .sum(sum[2]), .cOut(c3)   );
    onebitadder a4(.cIn(c3),  .a(a[3]), .b(b[3]), .sum(sum[3]), .cOut(cOut) );
endmodule
