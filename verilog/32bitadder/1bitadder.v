module onebitadder(cIn,a,b,sum,cOut,w1,w2,w3);
    input cIn,a,b;
    output sum,cOut,w1,w2,w3;
    
    wire w1,w2,w3;
    
    xor(w1,a,b);
    xor(sum, w1, cIn);
    and(w2, cIn, w1);
    and(w3,a,b);
    or(cOut, w3,w2);
endmodule
