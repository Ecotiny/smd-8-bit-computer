`include "4bitadder.v"

module sixteenbitadder(cIn, a, b, sum, cOut);
    input cIn;
    input [15:0] a;
    input [15:0] b;
    output [15:0] sum;
    output cOut;
    
    wire c1,c2,c3;
    
    fourbitadder a1(.cIn(cIn), .a(a[3:0]),   .b(b[3:0]),   .sum(sum[3:0]),   .cOut(c1)   );
    fourbitadder a2(.cIn(c1),  .a(a[7:4]),   .b(b[7:4]),   .sum(sum[7:4]),   .cOut(c2)   );
    fourbitadder a3(.cIn(c2),  .a(a[11:8]),  .b(b[11:8]),  .sum(sum[11:8]),  .cOut(c3)   );
    fourbitadder a4(.cIn(c3),  .a(a[15:12]), .b(b[15:12]), .sum(sum[15:12]), .cOut(cOut) );
endmodule
