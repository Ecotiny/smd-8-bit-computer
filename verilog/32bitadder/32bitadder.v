`include "16bitadder.v"

module thirtytwobitadder(cIn, a, b, sum, cOut);
    input cIn;
    input [31:0] a;
    input [31:0] b;
    output [31:0] sum;
    output cOut;
    
    wire c1;
    
    sixteenbitadder a1(.cIn(cIn), .a(a[15:0]),    .b(b[15:0]),    .sum(sum[15:0]),    .cOut(c1)     );
    sixteenbitadder a2(.cIn(c1),  .a(a[31:16]),   .b(b[31:16]),   .sum(sum[31:16]),   .cOut(cOut)   );
endmodule
