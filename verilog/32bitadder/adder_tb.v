`include "32bitadder.v"

module adder_tb();
    reg [31:0] a;
    reg [31:0] b;
    reg cIn;
    
    wire [31:0] sumOut;
    wire cOut;
    
    thirtytwobitadder atest(.cIn(cIn), .a(a), .b(b), .sum(sumOut), .cOut(cOut));
    
    initial begin
        a = 32'b10101001110101001110101011110101;
        b = 32'b11010101010101010101000000100001;
        cIn = 0;
        
        #20 $display("%d + %d = %b%b",a,b,cOut,sumOut);
        
        #30 $finish();
    end
endmodule

//16 Bit test
// `include "16bitadder.v"
// 
// module adder_tb();
//     reg [15:0] a;
//     reg [15:0] b;
//     reg cIn;
//     
//     wire [15:0] sumOut;
//     wire cOut;
//     
//     sixteenbitadder atest(.cIn(cIn), .a(a), .b(b), .sum(sumOut), .cOut(cOut));
//     
//     initial begin
//         a = 16'd1250;
//         b = 16'd33234;
//         cIn = 0;
//         
//         #20 $display("%b + %b = %b %b",a,b,cOut,sumOut);
//         
//         #30 $finish();
//     end
//  endmodule

//4 Bit test
// `include "4bitadder.v"
// 
// module adder_tb();
//     reg [3:0] a;
//     reg [3:0] b;
//     reg cIn;
//     
//     wire [3:0] sumOut;
//     wire cOut;
//     
//     fourbitadder atest(.cIn(cIn), .a(a), .b(b), .sum(sumOut), .cOut(cOut));
//     
//     initial begin
//         a = 4'b0111;
//         b = 4'b0001;
//         cIn = 0;
//         
//         #40 $display("%b + %b = %b%b",a,b,cOut,sumOut);
//         
//         #50 $finish();
//     end
//  endmodule

//One bit test
// `include "1bitadder.v"
// 
// module adder_tb();
//     reg a;
//     reg b;
//     reg cIn;
//     
//     wire sumOut;
//     wire cOut;
//     wire w1,w2,w3;
//     
//     onebitadder atest(.cIn(cIn), .a(a), .b(b), .sum(sumOut), .cOut(cOut), .w1(w1), .w2(w2),.w3(w3));
//     
//     initial begin
//         a = 1;
//         b = 0;
//         cIn = 0;
//         
//         #40 $display("%b + %b = %b%b\n=====\n%b %b %b",a,b,cOut,sumOut,w1, w2, w3);
//         
//         #50 $finish();
//     end
//  endmodule
