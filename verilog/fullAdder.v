module addFull(carryIn, a, b, sum, carryOut);
    input a,b,carryIn;
    output sum,carryOut;
    
    wire w1,w2,w3;
    xor(w1,a,b);
    xor(sum,w1,carryIn);
    and(w2,w1,carryIn);
    and(w3,a,b);
    or(carryOut,w2,w3);
endmodule
