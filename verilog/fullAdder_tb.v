`include "fullAdder.v"

module fullAdder_tb();
    
    reg cIn;
    reg a, b;
    
    wire sum, carryOut;
    
    
    addFull foo(.carryIn(cIn), .a(a), .b(b), .sum(sum), .carryOut(carryOut));
    
    
    initial begin
        $display("Hello");
        cIn = 0;
        a = 1;
        b = 1;
        
        $display("%b + %b = %b%b", a, b, carryOut, sum);
        
        #10 $finish();
    end
endmodule
