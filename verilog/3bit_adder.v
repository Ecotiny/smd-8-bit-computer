`include "fullAdder.v"

module add3(cIn, a,b,sum,cOut);
    input cIn;
    input [2:0] a;
    input [2:0] b;
    output [2:0] sum;
    output cOut;
    
    wire cIn1, cIn2;
    
    addFull a1 (.carryIn(cIn), .a(a[0]), .b(b[0]), .sum(sum[0]), .carryOut(cIn1));
    
    addFull a2 (.carryIn(cIn1), .a(a[1]), .b(b[1]), .sum(sum[1]), .carryOut(cIn2));
    
    addFull a3 (.carryIn(cIn2), .a(a[2]), .b(b[2]), .sum(sum[2]), .carryOut(cOut));

endmodule
    
