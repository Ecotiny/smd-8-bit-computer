`include "3bit_adder.v"

module bit3_adder_tb();
    reg [2:0] a;
    reg [2:0] b;
    wire [2:0] out;
    reg cin;
    wire cout;
    
    add3 tester(.cIn(cin), .a(a), .b(b), .sum(out), .cOut(cout));
    
    initial begin
        a = 3'b110;
        b = 3'b010;
        cin = 0;
        
        #10 $display("%b + %b = %b%b", a, b, cout, out);
        
        #10 $finish();
    end
endmodule
        
