import sys, os
# Add the home directory to sys.path
sys.path.append("/home/mmccoo/kicad/kicad/install/lib/python2.7/dist-packages")


startx = 32.8 
starty = 35.6 
endx = 44.2 
endy = 48.2
width = endx - startx
height = endy - starty

infilename = "/home/linus/git/smd-8-bit-computer/transistor-computer/nand.kicad_pcb"
outfilename = "/home/linus/git/smd-8-bit-computer/transistor-computer/nand_panelised.kicad_pcb"

array_w = 9
array_h = 9



import pcbnew



#main_board = pcbnew.GetBoard()
print("loading main")
main_board = pcbnew.LoadBoard("/home/linus/empty.kicad_pcb")
print("loading other")

other_board = infilename

def append_board(main_board, other_board_file, offset):

    offset = pcbnew.wxPoint(pcbnew.Millimeter2iu(offset[0]), pcbnew.Millimeter2iu(offset[1]))
    
    other_board =  pcbnew.LoadBoard(other_board_file)

    for mod in list(other_board.GetModules()):
        main_board.Add(mod)
        mod.Move(offset)

    for net in list(other_board.GetNetsByName().values()):
        main_board.Add(net)

    for track in list(other_board.GetTracks()):
        main_board.Add(track)
        track.Move(offset)

    for zone in list(other_board.Zones()):
        main_board.Add(zone)
        zone.Move(offset)

pcbnew.Refresh()


for y in range(array_h):
    for x in range(array_w):
        append_board(main_board, other_board, (startx + (x * width), starty + (y * height)))

print("saving")
pcbnew.SaveBoard(outfilename, main_board)

print("done")
