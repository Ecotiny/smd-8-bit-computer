startx = 32.8 
starty = 35.6 
endx = 44.2 
endy = 48.2
width = endx - startx
height = endy - starty

infilename = "nand.kicad_pcb"
outfilename = "nand_panelised.kicad_pcb"

array_w = 9
array_h = 9

with open("configfile", "w+") as f:
    f.write("load {}\n".format(infilename))
    f.write("compat latest\n")
    f.write("source-area {} {} {} {}\n".format(startx, starty, endx, endy))
    for y in range(array_h):
        for x in range(array_w):
            f.write("copy {} {}\n".format(startx + (x * width), starty + (y * height)))
    f.write("save {}\n".format(outfilename))
