EESchema Schematic File Version 4
LIBS:motherboard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3100 5350 850  1000
U 5C577569
F0 "ZF Calculation" 50
F1 "zf_calc.sch" 50
F2 "ZF" O L 3100 5850 50 
F3 "A" I R 3950 6300 50 
F4 "B" I R 3950 6200 50 
F5 "C" I R 3950 6100 50 
F6 "D" I R 3950 6000 50 
F7 "E" I R 3950 5900 50 
F8 "F" I R 3950 5800 50 
F9 "G" I R 3950 5700 50 
F10 "H" I R 3950 5600 50 
F11 "I" I R 3950 5500 50 
F12 "J" I R 3950 5400 50 
$EndSheet
$Comp
L Connector:TestPoint TP1
U 1 1 5C58134A
P 10600 1500
F 0 "TP1" V 10554 1688 50  0000 L CNN
F 1 "TestPoint" V 10645 1688 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 1500 50  0001 C CNN
F 3 "~" H 10800 1500 50  0001 C CNN
	1    10600 1500
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5C5813B0
P 10600 1700
F 0 "TP2" V 10554 1888 50  0000 L CNN
F 1 "TestPoint" V 10645 1888 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 1700 50  0001 C CNN
F 3 "~" H 10800 1700 50  0001 C CNN
	1    10600 1700
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5C5813C3
P 10600 1900
F 0 "TP3" V 10554 2088 50  0000 L CNN
F 1 "TestPoint" V 10645 2088 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 1900 50  0001 C CNN
F 3 "~" H 10800 1900 50  0001 C CNN
	1    10600 1900
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 5C5813DE
P 10600 2100
F 0 "TP4" V 10554 2288 50  0000 L CNN
F 1 "TestPoint" V 10645 2288 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 2100 50  0001 C CNN
F 3 "~" H 10800 2100 50  0001 C CNN
	1    10600 2100
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP5
U 1 1 5C5813F3
P 10600 2300
F 0 "TP5" V 10554 2488 50  0000 L CNN
F 1 "TestPoint" V 10645 2488 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 2300 50  0001 C CNN
F 3 "~" H 10800 2300 50  0001 C CNN
	1    10600 2300
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP6
U 1 1 5C58140A
P 10600 2500
F 0 "TP6" V 10554 2688 50  0000 L CNN
F 1 "TestPoint" V 10645 2688 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 2500 50  0001 C CNN
F 3 "~" H 10800 2500 50  0001 C CNN
	1    10600 2500
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP7
U 1 1 5C58143B
P 10600 2700
F 0 "TP7" V 10554 2888 50  0000 L CNN
F 1 "TestPoint" V 10645 2888 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 2700 50  0001 C CNN
F 3 "~" H 10800 2700 50  0001 C CNN
	1    10600 2700
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP8
U 1 1 5C581493
P 10600 2900
F 0 "TP8" V 10554 3088 50  0000 L CNN
F 1 "TestPoint" V 10645 3088 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 2900 50  0001 C CNN
F 3 "~" H 10800 2900 50  0001 C CNN
	1    10600 2900
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP9
U 1 1 5C581502
P 10600 3100
F 0 "TP9" V 10554 3288 50  0000 L CNN
F 1 "TestPoint" V 10645 3288 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 3100 50  0001 C CNN
F 3 "~" H 10800 3100 50  0001 C CNN
	1    10600 3100
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP10
U 1 1 5C58153B
P 10600 3300
F 0 "TP10" V 10554 3488 50  0000 L CNN
F 1 "TestPoint" V 10645 3488 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 3300 50  0001 C CNN
F 3 "~" H 10800 3300 50  0001 C CNN
	1    10600 3300
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP11
U 1 1 5C581B45
P 10600 3900
F 0 "TP11" V 10554 4088 50  0000 L CNN
F 1 "TestPoint" V 10645 4088 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 3900 50  0001 C CNN
F 3 "~" H 10800 3900 50  0001 C CNN
	1    10600 3900
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP12
U 1 1 5C581B4C
P 10600 4100
F 0 "TP12" V 10554 4288 50  0000 L CNN
F 1 "TestPoint" V 10645 4288 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 4100 50  0001 C CNN
F 3 "~" H 10800 4100 50  0001 C CNN
	1    10600 4100
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP13
U 1 1 5C581B53
P 10600 4300
F 0 "TP13" V 10554 4488 50  0000 L CNN
F 1 "TestPoint" V 10645 4488 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 4300 50  0001 C CNN
F 3 "~" H 10800 4300 50  0001 C CNN
	1    10600 4300
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP14
U 1 1 5C581B5A
P 10600 4500
F 0 "TP14" V 10554 4688 50  0000 L CNN
F 1 "TestPoint" V 10645 4688 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 4500 50  0001 C CNN
F 3 "~" H 10800 4500 50  0001 C CNN
	1    10600 4500
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP15
U 1 1 5C581B61
P 10600 4700
F 0 "TP15" V 10554 4888 50  0000 L CNN
F 1 "TestPoint" V 10645 4888 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 4700 50  0001 C CNN
F 3 "~" H 10800 4700 50  0001 C CNN
	1    10600 4700
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP16
U 1 1 5C581B68
P 10600 4900
F 0 "TP16" V 10554 5088 50  0000 L CNN
F 1 "TestPoint" V 10645 5088 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 4900 50  0001 C CNN
F 3 "~" H 10800 4900 50  0001 C CNN
	1    10600 4900
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP17
U 1 1 5C581B6F
P 10600 5100
F 0 "TP17" V 10554 5288 50  0000 L CNN
F 1 "TestPoint" V 10645 5288 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 5100 50  0001 C CNN
F 3 "~" H 10800 5100 50  0001 C CNN
	1    10600 5100
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP18
U 1 1 5C581B76
P 10600 5300
F 0 "TP18" V 10554 5488 50  0000 L CNN
F 1 "TestPoint" V 10645 5488 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 5300 50  0001 C CNN
F 3 "~" H 10800 5300 50  0001 C CNN
	1    10600 5300
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP19
U 1 1 5C581B7D
P 10600 5500
F 0 "TP19" V 10554 5688 50  0000 L CNN
F 1 "TestPoint" V 10645 5688 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 5500 50  0001 C CNN
F 3 "~" H 10800 5500 50  0001 C CNN
	1    10600 5500
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP20
U 1 1 5C581B84
P 10600 5700
F 0 "TP20" V 10554 5888 50  0000 L CNN
F 1 "TestPoint" V 10645 5888 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 5700 50  0001 C CNN
F 3 "~" H 10800 5700 50  0001 C CNN
	1    10600 5700
	0    1    1    0   
$EndComp
Text Label 10850 3750 0    98   ~ 0
A
Text Label 10850 1350 0    98   ~ 0
B
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5C582AB5
P 9050 1000
F 0 "H2" H 9150 1051 50  0000 L CNN
F 1 "Sub" H 9150 960 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 9050 1000 50  0001 C CNN
F 3 "~" H 9050 1000 50  0001 C CNN
	1    9050 1000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5C582F7D
P 9350 900
F 0 "H1" H 9250 900 50  0000 R CNN
F 1 "VCC" H 9250 1000 50  0000 R CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 9350 900 50  0001 C CNN
F 3 "~" H 9350 900 50  0001 C CNN
	1    9350 900 
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 5C583044
P 9350 800
F 0 "#PWR01" H 9350 650 50  0001 C CNN
F 1 "VCC" H 9367 973 50  0000 C CNN
F 2 "" H 9350 800 50  0001 C CNN
F 3 "" H 9350 800 50  0001 C CNN
	1    9350 800 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5C583266
P 8700 1000
F 0 "H3" H 8500 1050 50  0000 L CNN
F 1 "GND" H 8500 950 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 8700 1000 50  0001 C CNN
F 3 "~" H 8700 1000 50  0001 C CNN
	1    8700 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5C583677
P 8700 1100
F 0 "#PWR02" H 8700 850 50  0001 C CNN
F 1 "GND" H 8705 927 50  0000 C CNN
F 2 "" H 8700 1100 50  0001 C CNN
F 3 "" H 8700 1100 50  0001 C CNN
	1    8700 1100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP21
U 1 1 5C583AC2
P 9700 1500
F 0 "TP21" V 9600 1800 50  0000 C CNN
F 1 "TestPoint" V 9700 1850 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 9900 1500 50  0001 C CNN
F 3 "~" H 9900 1500 50  0001 C CNN
	1    9700 1500
	0    -1   1    0   
$EndComp
Wire Wire Line
	9700 1500 10600 1500
Wire Wire Line
	9700 1700 10600 1700
Wire Wire Line
	9700 1900 10600 1900
Wire Wire Line
	9700 2100 10600 2100
Wire Wire Line
	9700 2300 10600 2300
Wire Wire Line
	9700 2500 10600 2500
Wire Wire Line
	9700 2700 10600 2700
Wire Wire Line
	10600 2900 9700 2900
Wire Wire Line
	9700 3100 10600 3100
$Comp
L Connector:TestPoint TP22
U 1 1 5C584C1C
P 9700 1700
F 0 "TP22" V 9600 2000 50  0000 C CNN
F 1 "TestPoint" V 9700 2050 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 9900 1700 50  0001 C CNN
F 3 "~" H 9900 1700 50  0001 C CNN
	1    9700 1700
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP23
U 1 1 5C584D20
P 9700 1900
F 0 "TP23" V 9600 2200 50  0000 C CNN
F 1 "TestPoint" V 9700 2250 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 9900 1900 50  0001 C CNN
F 3 "~" H 9900 1900 50  0001 C CNN
	1    9700 1900
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP24
U 1 1 5C5851EC
P 9700 2100
F 0 "TP24" V 9600 2400 50  0000 C CNN
F 1 "TestPoint" V 9700 2450 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 9900 2100 50  0001 C CNN
F 3 "~" H 9900 2100 50  0001 C CNN
	1    9700 2100
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP25
U 1 1 5C5852DE
P 9700 2300
F 0 "TP25" V 9600 2600 50  0000 C CNN
F 1 "TestPoint" V 9700 2650 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 9900 2300 50  0001 C CNN
F 3 "~" H 9900 2300 50  0001 C CNN
	1    9700 2300
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP26
U 1 1 5C5853D0
P 9700 2500
F 0 "TP26" V 9600 2800 50  0000 C CNN
F 1 "TestPoint" V 9700 2850 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 9900 2500 50  0001 C CNN
F 3 "~" H 9900 2500 50  0001 C CNN
	1    9700 2500
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP27
U 1 1 5C58550A
P 9700 2700
F 0 "TP27" V 9600 3000 50  0000 C CNN
F 1 "TestPoint" V 9700 3050 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 9900 2700 50  0001 C CNN
F 3 "~" H 9900 2700 50  0001 C CNN
	1    9700 2700
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP28
U 1 1 5C5855FE
P 9700 2900
F 0 "TP28" V 9600 3200 50  0000 C CNN
F 1 "TestPoint" V 9700 3250 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 9900 2900 50  0001 C CNN
F 3 "~" H 9900 2900 50  0001 C CNN
	1    9700 2900
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP29
U 1 1 5C5856F4
P 9700 3100
F 0 "TP29" V 9600 3400 50  0000 C CNN
F 1 "TestPoint" V 9700 3450 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 9900 3100 50  0001 C CNN
F 3 "~" H 9900 3100 50  0001 C CNN
	1    9700 3100
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP30
U 1 1 5C5857F0
P 9700 3300
F 0 "TP30" V 9600 3600 50  0000 C CNN
F 1 "TestPoint" V 9700 3650 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 9900 3300 50  0001 C CNN
F 3 "~" H 9900 3300 50  0001 C CNN
	1    9700 3300
	0    -1   1    0   
$EndComp
Wire Wire Line
	9700 3300 10600 3300
Wire Notes Line
	9800 3450 9800 550 
Wire Notes Line
	9800 550  8400 550 
Wire Notes Line
	8400 550  8400 3450
Wire Notes Line
	8400 3450 9800 3450
Text Label 8800 800  0    98   ~ 0
XOR
$Comp
L Connector:TestPoint TP31
U 1 1 5C589D10
P 8450 1450
F 0 "TP31" V 8404 1638 50  0000 L CNN
F 1 "TestPoint" V 8495 1638 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 8650 1450 50  0001 C CNN
F 3 "~" H 8650 1450 50  0001 C CNN
	1    8450 1450
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP32
U 1 1 5C589D17
P 8450 1650
F 0 "TP32" V 8404 1838 50  0000 L CNN
F 1 "TestPoint" V 8495 1838 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 8650 1650 50  0001 C CNN
F 3 "~" H 8650 1650 50  0001 C CNN
	1    8450 1650
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP33
U 1 1 5C589D1E
P 8450 1850
F 0 "TP33" V 8404 2038 50  0000 L CNN
F 1 "TestPoint" V 8495 2038 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 8650 1850 50  0001 C CNN
F 3 "~" H 8650 1850 50  0001 C CNN
	1    8450 1850
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP34
U 1 1 5C589D25
P 8450 2050
F 0 "TP34" V 8404 2238 50  0000 L CNN
F 1 "TestPoint" V 8495 2238 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 8650 2050 50  0001 C CNN
F 3 "~" H 8650 2050 50  0001 C CNN
	1    8450 2050
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP35
U 1 1 5C589D2C
P 8450 2250
F 0 "TP35" V 8404 2438 50  0000 L CNN
F 1 "TestPoint" V 8495 2438 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 8650 2250 50  0001 C CNN
F 3 "~" H 8650 2250 50  0001 C CNN
	1    8450 2250
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP36
U 1 1 5C589D33
P 8450 2450
F 0 "TP36" V 8404 2638 50  0000 L CNN
F 1 "TestPoint" V 8495 2638 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 8650 2450 50  0001 C CNN
F 3 "~" H 8650 2450 50  0001 C CNN
	1    8450 2450
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP37
U 1 1 5C589D3A
P 8450 2650
F 0 "TP37" V 8404 2838 50  0000 L CNN
F 1 "TestPoint" V 8495 2838 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 8650 2650 50  0001 C CNN
F 3 "~" H 8650 2650 50  0001 C CNN
	1    8450 2650
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP38
U 1 1 5C589D41
P 8450 2850
F 0 "TP38" V 8404 3038 50  0000 L CNN
F 1 "TestPoint" V 8495 3038 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 8650 2850 50  0001 C CNN
F 3 "~" H 8650 2850 50  0001 C CNN
	1    8450 2850
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP39
U 1 1 5C589D48
P 8450 3050
F 0 "TP39" V 8404 3238 50  0000 L CNN
F 1 "TestPoint" V 8495 3238 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 8650 3050 50  0001 C CNN
F 3 "~" H 8650 3050 50  0001 C CNN
	1    8450 3050
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP40
U 1 1 5C589D4F
P 8450 3250
F 0 "TP40" V 8404 3438 50  0000 L CNN
F 1 "TestPoint" V 8495 3438 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 8650 3250 50  0001 C CNN
F 3 "~" H 8650 3250 50  0001 C CNN
	1    8450 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	7900 3250 8450 3250
$Comp
L Connector:TestPoint TP41
U 1 1 5C598DF8
P 6750 1450
F 0 "TP41" V 6650 1750 50  0000 C CNN
F 1 "TestPoint" V 6750 1800 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 1450 50  0001 C CNN
F 3 "~" H 6950 1450 50  0001 C CNN
	1    6750 1450
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP42
U 1 1 5C598DFF
P 6750 1650
F 0 "TP42" V 6650 1950 50  0000 C CNN
F 1 "TestPoint" V 6750 2000 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 1650 50  0001 C CNN
F 3 "~" H 6950 1650 50  0001 C CNN
	1    6750 1650
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP43
U 1 1 5C598E06
P 6750 1900
F 0 "TP43" V 6650 2200 50  0000 C CNN
F 1 "TestPoint" V 6750 2250 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 1900 50  0001 C CNN
F 3 "~" H 6950 1900 50  0001 C CNN
	1    6750 1900
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP44
U 1 1 5C598E0D
P 6750 2100
F 0 "TP44" V 6650 2400 50  0000 C CNN
F 1 "TestPoint" V 6750 2450 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 2100 50  0001 C CNN
F 3 "~" H 6950 2100 50  0001 C CNN
	1    6750 2100
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP45
U 1 1 5C598E14
P 6750 2350
F 0 "TP45" V 6650 2650 50  0000 C CNN
F 1 "TestPoint" V 6750 2700 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 2350 50  0001 C CNN
F 3 "~" H 6950 2350 50  0001 C CNN
	1    6750 2350
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP46
U 1 1 5C598E1B
P 6750 2550
F 0 "TP46" V 6650 2850 50  0000 C CNN
F 1 "TestPoint" V 6750 2900 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 2550 50  0001 C CNN
F 3 "~" H 6950 2550 50  0001 C CNN
	1    6750 2550
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP47
U 1 1 5C598E22
P 6750 2800
F 0 "TP47" V 6650 3100 50  0000 C CNN
F 1 "TestPoint" V 6750 3150 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 2800 50  0001 C CNN
F 3 "~" H 6950 2800 50  0001 C CNN
	1    6750 2800
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP48
U 1 1 5C598E29
P 6750 3000
F 0 "TP48" V 6650 3300 50  0000 C CNN
F 1 "TestPoint" V 6750 3350 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 3000 50  0001 C CNN
F 3 "~" H 6950 3000 50  0001 C CNN
	1    6750 3000
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP49
U 1 1 5C598E30
P 6750 3250
F 0 "TP49" V 6650 3550 50  0000 C CNN
F 1 "TestPoint" V 6750 3600 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 3250 50  0001 C CNN
F 3 "~" H 6950 3250 50  0001 C CNN
	1    6750 3250
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP50
U 1 1 5C598E37
P 6750 3450
F 0 "TP50" V 6650 3750 50  0000 C CNN
F 1 "TestPoint" V 6750 3800 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 3450 50  0001 C CNN
F 3 "~" H 6950 3450 50  0001 C CNN
	1    6750 3450
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP51
U 1 1 5C59CDFD
P 6750 3700
F 0 "TP51" V 6650 4000 50  0000 C CNN
F 1 "TestPoint" V 6750 4050 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 3700 50  0001 C CNN
F 3 "~" H 6950 3700 50  0001 C CNN
	1    6750 3700
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP52
U 1 1 5C59CE04
P 6750 3900
F 0 "TP52" V 6650 4200 50  0000 C CNN
F 1 "TestPoint" V 6750 4250 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 3900 50  0001 C CNN
F 3 "~" H 6950 3900 50  0001 C CNN
	1    6750 3900
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP53
U 1 1 5C59CE0B
P 6750 4150
F 0 "TP53" V 6650 4450 50  0000 C CNN
F 1 "TestPoint" V 6750 4500 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 4150 50  0001 C CNN
F 3 "~" H 6950 4150 50  0001 C CNN
	1    6750 4150
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP54
U 1 1 5C59CE12
P 6750 4350
F 0 "TP54" V 6650 4650 50  0000 C CNN
F 1 "TestPoint" V 6750 4700 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 4350 50  0001 C CNN
F 3 "~" H 6950 4350 50  0001 C CNN
	1    6750 4350
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP55
U 1 1 5C59CE19
P 6750 4600
F 0 "TP55" V 6650 4900 50  0000 C CNN
F 1 "TestPoint" V 6750 4950 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 4600 50  0001 C CNN
F 3 "~" H 6950 4600 50  0001 C CNN
	1    6750 4600
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP56
U 1 1 5C59CE20
P 6750 4800
F 0 "TP56" V 6650 5100 50  0000 C CNN
F 1 "TestPoint" V 6750 5150 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 4800 50  0001 C CNN
F 3 "~" H 6950 4800 50  0001 C CNN
	1    6750 4800
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP57
U 1 1 5C59CE27
P 6750 5050
F 0 "TP57" V 6650 5350 50  0000 C CNN
F 1 "TestPoint" V 6750 5400 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 5050 50  0001 C CNN
F 3 "~" H 6950 5050 50  0001 C CNN
	1    6750 5050
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP58
U 1 1 5C59CE2E
P 6750 5250
F 0 "TP58" V 6650 5550 50  0000 C CNN
F 1 "TestPoint" V 6750 5600 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 5250 50  0001 C CNN
F 3 "~" H 6950 5250 50  0001 C CNN
	1    6750 5250
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP59
U 1 1 5C59CE35
P 6750 5500
F 0 "TP59" V 6650 5800 50  0000 C CNN
F 1 "TestPoint" V 6750 5850 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 5500 50  0001 C CNN
F 3 "~" H 6950 5500 50  0001 C CNN
	1    6750 5500
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP60
U 1 1 5C59CE3C
P 6750 5700
F 0 "TP60" V 6650 6000 50  0000 C CNN
F 1 "TestPoint" V 6750 6050 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 6950 5700 50  0001 C CNN
F 3 "~" H 6950 5700 50  0001 C CNN
	1    6750 5700
	0    -1   1    0   
$EndComp
Wire Wire Line
	7500 1900 6750 1900
Wire Wire Line
	7500 1650 8450 1650
Wire Wire Line
	7000 1650 6750 1650
Wire Wire Line
	7050 2100 6750 2100
Wire Wire Line
	7100 2550 6750 2550
Wire Wire Line
	7050 4100 10600 4100
Wire Wire Line
	7100 4300 10600 4300
Wire Wire Line
	7150 3000 6750 3000
Wire Wire Line
	7150 4500 10600 4500
Wire Wire Line
	7200 4700 10600 4700
Wire Wire Line
	7200 3450 6750 3450
Wire Wire Line
	7250 4900 10600 4900
Wire Wire Line
	7300 4350 6750 4350
Wire Wire Line
	7300 5100 10600 5100
Wire Wire Line
	7350 4750 7200 4750
Wire Wire Line
	7200 4750 7200 4800
Wire Wire Line
	7200 4800 6750 4800
Wire Wire Line
	7350 5300 10600 5300
Wire Wire Line
	7400 5250 6750 5250
Wire Wire Line
	7850 5050 6750 5050
Wire Wire Line
	7850 3050 8450 3050
Wire Wire Line
	7800 4550 7150 4550
Wire Wire Line
	7150 4550 7150 4600
Wire Wire Line
	7150 4600 6750 4600
Wire Wire Line
	7800 2850 8450 2850
Wire Wire Line
	7750 4150 6750 4150
Wire Wire Line
	7750 2650 8450 2650
Wire Wire Line
	7700 3700 6750 3700
Wire Wire Line
	7700 2450 8450 2450
Wire Wire Line
	7650 3250 6750 3250
Wire Wire Line
	7650 2250 8450 2250
Wire Wire Line
	7600 2800 6750 2800
Wire Wire Line
	7600 2050 8450 2050
Wire Wire Line
	7550 2350 6750 2350
Wire Wire Line
	7550 1850 8450 1850
$Comp
L Connector:TestPoint TP61
U 1 1 5C5DF2D1
P 5550 2900
F 0 "TP61" V 5450 3200 50  0000 C CNN
F 1 "TestPoint" V 5550 3250 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 5750 2900 50  0001 C CNN
F 3 "~" H 5750 2900 50  0001 C CNN
	1    5550 2900
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP62
U 1 1 5C5DF2D8
P 5550 3100
F 0 "TP62" V 5450 3400 50  0000 C CNN
F 1 "TestPoint" V 5550 3450 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 5750 3100 50  0001 C CNN
F 3 "~" H 5750 3100 50  0001 C CNN
	1    5550 3100
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP63
U 1 1 5C5DF2DF
P 5550 3300
F 0 "TP63" V 5450 3600 50  0000 C CNN
F 1 "TestPoint" V 5550 3650 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 5750 3300 50  0001 C CNN
F 3 "~" H 5750 3300 50  0001 C CNN
	1    5550 3300
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP64
U 1 1 5C5DF2E6
P 5550 3500
F 0 "TP64" V 5450 3800 50  0000 C CNN
F 1 "TestPoint" V 5550 3850 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 5750 3500 50  0001 C CNN
F 3 "~" H 5750 3500 50  0001 C CNN
	1    5550 3500
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP65
U 1 1 5C5DF2ED
P 5550 3700
F 0 "TP65" V 5450 4000 50  0000 C CNN
F 1 "TestPoint" V 5550 4050 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 5750 3700 50  0001 C CNN
F 3 "~" H 5750 3700 50  0001 C CNN
	1    5550 3700
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP66
U 1 1 5C5DF2F4
P 5550 3900
F 0 "TP66" V 5450 4200 50  0000 C CNN
F 1 "TestPoint" V 5550 4250 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 5750 3900 50  0001 C CNN
F 3 "~" H 5750 3900 50  0001 C CNN
	1    5550 3900
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP67
U 1 1 5C5DF2FB
P 5550 4100
F 0 "TP67" V 5450 4400 50  0000 C CNN
F 1 "TestPoint" V 5550 4450 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 5750 4100 50  0001 C CNN
F 3 "~" H 5750 4100 50  0001 C CNN
	1    5550 4100
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP68
U 1 1 5C5DF302
P 5550 4300
F 0 "TP68" V 5450 4600 50  0000 C CNN
F 1 "TestPoint" V 5550 4650 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 5750 4300 50  0001 C CNN
F 3 "~" H 5750 4300 50  0001 C CNN
	1    5550 4300
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP69
U 1 1 5C5DF309
P 5550 4500
F 0 "TP69" V 5450 4800 50  0000 C CNN
F 1 "TestPoint" V 5550 4850 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 5750 4500 50  0001 C CNN
F 3 "~" H 5750 4500 50  0001 C CNN
	1    5550 4500
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP70
U 1 1 5C5DF310
P 5550 4700
F 0 "TP70" V 5450 5000 50  0000 C CNN
F 1 "TestPoint" V 5550 5050 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 5750 4700 50  0001 C CNN
F 3 "~" H 5750 4700 50  0001 C CNN
	1    5550 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	7900 3250 7900 5500
Wire Wire Line
	7850 3050 7850 5050
Wire Wire Line
	7800 2850 7800 4550
Wire Wire Line
	7750 2650 7750 4150
Wire Wire Line
	7700 2450 7700 3700
Wire Wire Line
	7650 2250 7650 3250
Wire Wire Line
	7600 2050 7600 2800
Wire Wire Line
	7550 1850 7550 2350
Wire Wire Line
	7500 1650 7500 1900
Wire Wire Line
	6750 1450 8450 1450
Wire Wire Line
	6750 3900 7250 3900
Wire Wire Line
	6750 5500 7900 5500
Wire Wire Line
	7050 2100 7050 4100
Wire Wire Line
	7250 3900 7250 4900
Wire Wire Line
	6750 5700 10600 5700
Wire Wire Line
	10600 5500 7950 5500
Wire Wire Line
	7950 5500 7950 5450
Wire Wire Line
	7950 5450 7400 5450
Wire Wire Line
	7400 5250 7400 5450
Wire Wire Line
	7350 4750 7350 5300
Wire Wire Line
	7300 4350 7300 5100
Wire Wire Line
	7150 3000 7150 4500
Wire Wire Line
	7200 3450 7200 4700
Wire Wire Line
	7100 2550 7100 4300
Wire Wire Line
	7300 3900 7300 3850
Wire Wire Line
	7300 3850 7000 3850
Wire Wire Line
	7000 1650 7000 3850
Wire Wire Line
	7300 3900 10600 3900
Wire Notes Line
	6100 2450 5400 2450
Wire Notes Line
	5400 1300 5400 5750
Wire Notes Line
	5400 4800 6100 4800
Wire Notes Line
	6100 2450 6100 4800
Text Label 5450 2650 0    98   ~ 0
Sum_out
Wire Wire Line
	9050 1100 10600 1100
Wire Wire Line
	5550 4700 5350 4700
Wire Wire Line
	5550 4500 5250 4500
Wire Wire Line
	5550 4300 5150 4300
Wire Wire Line
	5050 4100 5550 4100
Wire Wire Line
	5550 3900 4950 3900
Wire Wire Line
	4850 3700 5550 3700
Wire Wire Line
	5550 3500 4750 3500
Wire Wire Line
	4650 3300 5550 3300
Wire Wire Line
	5550 3100 4550 3100
Wire Wire Line
	5550 2900 4450 2900
Wire Notes Line
	5400 1300 6800 1300
Wire Notes Line
	6800 1300 6800 5750
Wire Notes Line
	6800 5750 5400 5750
$Comp
L Mechanical:MountingHole_Pad H5
U 1 1 5C6A2F27
P 6100 1600
F 0 "H5" H 6300 1650 50  0000 R CNN
F 1 "VCC" V 6050 1550 50  0000 R CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 6100 1600 50  0001 C CNN
F 3 "~" H 6100 1600 50  0001 C CNN
	1    6100 1600
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H6
U 1 1 5C6A310A
P 5800 1850
F 0 "H6" H 5900 1901 50  0000 L CNN
F 1 "GND" V 5650 1800 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 5800 1850 50  0001 C CNN
F 3 "~" H 5800 1850 50  0001 C CNN
	1    5800 1850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H7
U 1 1 5C6A74B2
P 5500 2200
F 0 "H7" H 5600 2251 50  0000 L CNN
F 1 "C_in" H 5600 2160 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 5500 2200 50  0001 C CNN
F 3 "~" H 5500 2200 50  0001 C CNN
	1    5500 2200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5C6AB868
P 5850 1450
F 0 "H4" H 6050 1500 50  0000 R CNN
F 1 "VCC" V 5550 1550 50  0000 R CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 5850 1450 50  0001 C CNN
F 3 "~" H 5850 1450 50  0001 C CNN
	1    5850 1450
	-1   0    0    1   
$EndComp
Wire Wire Line
	6100 1500 6100 1350
Wire Wire Line
	6100 1350 5850 1350
$Comp
L power:VCC #PWR03
U 1 1 5C6C582D
P 6100 1350
F 0 "#PWR03" H 6100 1200 50  0001 C CNN
F 1 "VCC" H 6150 1400 50  0000 L CNN
F 2 "" H 6100 1350 50  0001 C CNN
F 3 "" H 6100 1350 50  0001 C CNN
	1    6100 1350
	0    1    1    0   
$EndComp
Connection ~ 6100 1350
$Comp
L power:GND #PWR04
U 1 1 5C6C64F2
P 5800 1950
F 0 "#PWR04" H 5800 1700 50  0001 C CNN
F 1 "GND" H 5900 1950 50  0000 C CNN
F 2 "" H 5800 1950 50  0001 C CNN
F 3 "" H 5800 1950 50  0001 C CNN
	1    5800 1950
	1    0    0    -1  
$EndComp
NoConn ~ 5500 2300
$Comp
L Connector:TestPoint TP82
U 1 1 5C6CFE9D
P 5550 5150
F 0 "TP82" V 5504 5337 50  0000 L CNN
F 1 "Carry_out" V 5595 5337 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 5750 5150 50  0001 C CNN
F 3 "~" H 5750 5150 50  0001 C CNN
	1    5550 5150
	0    1    1    0   
$EndComp
Connection ~ 5250 4500
Connection ~ 5350 4700
$Comp
L Connector:TestPoint TP81
U 1 1 5C712C25
P 10600 1100
F 0 "TP81" V 10554 1287 50  0000 L CNN
F 1 "Subtract" V 10645 1287 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 10800 1100 50  0001 C CNN
F 3 "~" H 10800 1100 50  0001 C CNN
	1    10600 1100
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H10
U 1 1 5C7C748D
P 1800 3700
F 0 "H10" H 1900 3751 50  0000 L CNN
F 1 "GND" H 1900 3660 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1800 3700 50  0001 C CNN
F 3 "~" H 1800 3700 50  0001 C CNN
	1    1800 3700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H9
U 1 1 5C7C772F
P 2200 4100
F 0 "H9" H 2100 4058 50  0000 R CNN
F 1 "VCC" H 2100 4149 50  0000 R CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 2200 4100 50  0001 C CNN
F 3 "~" H 2200 4100 50  0001 C CNN
	1    2200 4100
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H8
U 1 1 5C7C7CA4
P 1750 4150
F 0 "H8" V 1704 4299 50  0000 L CNN
F 1 "CLK" V 1795 4299 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1750 4150 50  0001 C CNN
F 3 "~" H 1750 4150 50  0001 C CNN
	1    1750 4150
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H11
U 1 1 5C7C824B
P 2250 3650
F 0 "H11" V 2400 3650 50  0000 C CNN
F 1 "~RST" V 2350 3500 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 2250 3650 50  0001 C CNN
F 3 "~" H 2250 3650 50  0001 C CNN
	1    2250 3650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5C7C8670
P 1800 3800
F 0 "#PWR0101" H 1800 3550 50  0001 C CNN
F 1 "GND" V 1700 3800 50  0000 C CNN
F 2 "" H 1800 3800 50  0001 C CNN
F 3 "" H 1800 3800 50  0001 C CNN
	1    1800 3800
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 5C7C8982
P 2200 4000
F 0 "#PWR0102" H 2200 3850 50  0001 C CNN
F 1 "VCC" V 2100 4000 50  0000 C CNN
F 2 "" H 2200 4000 50  0001 C CNN
F 3 "" H 2200 4000 50  0001 C CNN
	1    2200 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 3650 2550 3650
Wire Wire Line
	2550 3650 2550 3350
Wire Wire Line
	2550 3350 1500 3350
Wire Wire Line
	1650 4150 1500 4150
Text Label 1550 4150 0    49   ~ 0
CLK
Text Label 1550 3350 0    49   ~ 0
~RST
$Comp
L Connector:TestPoint TP86
U 1 1 5C7E338B
P 1500 3350
F 0 "TP86" V 1400 3650 50  0000 C CNN
F 1 "TestPoint" V 1500 3700 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 1700 3350 50  0001 C CNN
F 3 "~" H 1700 3350 50  0001 C CNN
	1    1500 3350
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP85
U 1 1 5C7E3392
P 1500 4150
F 0 "TP85" V 1400 4450 50  0000 C CNN
F 1 "TestPoint" V 1500 4500 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 1700 4150 50  0001 C CNN
F 3 "~" H 1700 4150 50  0001 C CNN
	1    1500 4150
	0    -1   1    0   
$EndComp
Wire Wire Line
	2650 5150 2650 4550
Wire Wire Line
	2650 4550 2300 4550
Wire Wire Line
	2650 5150 5550 5150
$Comp
L Connector:TestPoint TP84
U 1 1 5C804DF3
P 2300 4550
F 0 "TP84" V 2200 4850 50  0000 C CNN
F 1 "TestPoint" V 2300 4900 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 2500 4550 50  0001 C CNN
F 3 "~" H 2500 4550 50  0001 C CNN
	1    2300 4550
	0    -1   1    0   
$EndComp
Text Label 2450 4550 0    49   ~ 0
CF
Wire Wire Line
	3100 5850 2550 5850
Wire Wire Line
	2550 5850 2550 4800
Wire Wire Line
	2550 4800 2300 4800
$Comp
L Connector:TestPoint TP83
U 1 1 5C8261A1
P 2300 4800
F 0 "TP83" V 2200 5100 50  0000 C CNN
F 1 "TestPoint" V 2300 5150 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 2500 4800 50  0001 C CNN
F 3 "~" H 2500 4800 50  0001 C CNN
	1    2300 4800
	0    -1   1    0   
$EndComp
Text Label 2450 4800 0    49   ~ 0
ZF
$Comp
L Connector:TestPoint TP71
U 1 1 5C4BDA66
P 3600 700
F 0 "TP71" V 3500 1000 50  0000 C CNN
F 1 "TestPoint" V 3600 1050 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 3800 700 50  0001 C CNN
F 3 "~" H 3800 700 50  0001 C CNN
	1    3600 700 
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP72
U 1 1 5C4BDA6D
P 3600 1150
F 0 "TP72" V 3500 1450 50  0000 C CNN
F 1 "TestPoint" V 3600 1500 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 3800 1150 50  0001 C CNN
F 3 "~" H 3800 1150 50  0001 C CNN
	1    3600 1150
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP73
U 1 1 5C4BDA74
P 3600 1600
F 0 "TP73" V 3500 1900 50  0000 C CNN
F 1 "TestPoint" V 3600 1950 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 3800 1600 50  0001 C CNN
F 3 "~" H 3800 1600 50  0001 C CNN
	1    3600 1600
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP74
U 1 1 5C4BDA7B
P 3600 2050
F 0 "TP74" V 3500 2350 50  0000 C CNN
F 1 "TestPoint" V 3600 2400 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 3800 2050 50  0001 C CNN
F 3 "~" H 3800 2050 50  0001 C CNN
	1    3600 2050
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP75
U 1 1 5C4BDA82
P 3600 2500
F 0 "TP75" V 3500 2800 50  0000 C CNN
F 1 "TestPoint" V 3600 2850 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 3800 2500 50  0001 C CNN
F 3 "~" H 3800 2500 50  0001 C CNN
	1    3600 2500
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP76
U 1 1 5C4BDA89
P 3600 2950
F 0 "TP76" V 3500 3250 50  0000 C CNN
F 1 "TestPoint" V 3600 3300 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 3800 2950 50  0001 C CNN
F 3 "~" H 3800 2950 50  0001 C CNN
	1    3600 2950
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP77
U 1 1 5C4BDA90
P 3600 3400
F 0 "TP77" V 3500 3700 50  0000 C CNN
F 1 "TestPoint" V 3600 3750 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 3800 3400 50  0001 C CNN
F 3 "~" H 3800 3400 50  0001 C CNN
	1    3600 3400
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP78
U 1 1 5C4BDA97
P 3600 3850
F 0 "TP78" V 3500 4150 50  0000 C CNN
F 1 "TestPoint" V 3600 4200 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 3800 3850 50  0001 C CNN
F 3 "~" H 3800 3850 50  0001 C CNN
	1    3600 3850
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP79
U 1 1 5C4BDA9E
P 3600 4300
F 0 "TP79" V 3500 4600 50  0000 C CNN
F 1 "TestPoint" V 3600 4650 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 3800 4300 50  0001 C CNN
F 3 "~" H 3800 4300 50  0001 C CNN
	1    3600 4300
	0    -1   1    0   
$EndComp
$Comp
L Connector:TestPoint TP80
U 1 1 5C4BDAA5
P 3600 4750
F 0 "TP80" V 3500 5050 50  0000 C CNN
F 1 "TestPoint" V 3600 5100 50  0000 C CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 3800 4750 50  0001 C CNN
F 3 "~" H 3800 4750 50  0001 C CNN
	1    3600 4750
	0    -1   1    0   
$EndComp
Wire Wire Line
	5250 4500 5250 1150
Wire Wire Line
	5350 700  5350 4700
Wire Wire Line
	4450 6300 3950 6300
Wire Wire Line
	4450 2900 4450 4750
Wire Wire Line
	5350 4700 5350 5400
Wire Wire Line
	5350 5400 3950 5400
Wire Wire Line
	5250 5500 3950 5500
Wire Wire Line
	5250 4500 5250 5500
Wire Wire Line
	5150 4300 5150 5600
Wire Wire Line
	5150 5600 3950 5600
Wire Wire Line
	5050 5700 3950 5700
Wire Wire Line
	5050 4100 5050 5700
Wire Wire Line
	4950 3900 4950 5800
Wire Wire Line
	4950 5800 3950 5800
Wire Wire Line
	4850 3700 4850 5900
Wire Wire Line
	4850 5900 3950 5900
Wire Wire Line
	4750 3500 4750 6000
Wire Wire Line
	4750 6000 3950 6000
Wire Wire Line
	4650 3300 4650 3850
Wire Wire Line
	4650 6100 3950 6100
Wire Wire Line
	4550 3100 4550 4300
Wire Wire Line
	4550 6200 3950 6200
$Comp
L power:VCC #PWR05
U 1 1 5C636C72
P 3100 7200
F 0 "#PWR05" H 3100 7050 50  0001 C CNN
F 1 "VCC" V 3118 7327 50  0000 L CNN
F 2 "" H 3100 7200 50  0001 C CNN
F 3 "" H 3100 7200 50  0001 C CNN
	1    3100 7200
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR024
U 1 1 5C636DC4
P 3500 7200
F 0 "#PWR024" H 3500 6950 50  0001 C CNN
F 1 "GND" V 3505 7072 50  0000 R CNN
F 2 "" H 3500 7200 50  0001 C CNN
F 3 "" H 3500 7200 50  0001 C CNN
	1    3500 7200
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole H12
U 1 1 5C646CF9
P 1500 6550
F 0 "H12" H 1600 6596 50  0000 L CNN
F 1 "MountingHole" H 1600 6505 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1500 6550 50  0001 C CNN
F 3 "~" H 1500 6550 50  0001 C CNN
	1    1500 6550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H13
U 1 1 5C646F63
P 1500 6800
F 0 "H13" H 1600 6846 50  0000 L CNN
F 1 "MountingHole" H 1600 6755 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1500 6800 50  0001 C CNN
F 3 "~" H 1500 6800 50  0001 C CNN
	1    1500 6800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H14
U 1 1 5C64DD08
P 1500 7050
F 0 "H14" H 1600 7096 50  0000 L CNN
F 1 "MountingHole" H 1600 7005 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1500 7050 50  0001 C CNN
F 3 "~" H 1500 7050 50  0001 C CNN
	1    1500 7050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H15
U 1 1 5C654A9B
P 1500 7300
F 0 "H15" H 1600 7346 50  0000 L CNN
F 1 "MountingHole" H 1600 7255 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1500 7300 50  0001 C CNN
F 3 "~" H 1500 7300 50  0001 C CNN
	1    1500 7300
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP87
U 1 1 5C66393E
P 3100 7200
F 0 "TP87" H 3042 7227 50  0000 R CNN
F 1 "TestPoint" H 3042 7318 50  0000 R CNN
F 2 "TestPoint:TestPoint_Pad_4.0x4.0mm" H 3300 7200 50  0001 C CNN
F 3 "~" H 3300 7200 50  0001 C CNN
	1    3100 7200
	-1   0    0    1   
$EndComp
$Comp
L Connector:TestPoint TP88
U 1 1 5C671846
P 3500 7200
F 0 "TP88" H 3500 7500 50  0000 R CNN
F 1 "TestPoint" H 3600 7400 50  0000 R CNN
F 2 "TestPoint:TestPoint_Pad_4.0x4.0mm" H 3700 7200 50  0001 C CNN
F 3 "~" H 3700 7200 50  0001 C CNN
	1    3500 7200
	-1   0    0    -1  
$EndComp
$Comp
L linus:LOGO G1
U 1 1 5C673DBD
P 6250 7250
F 0 "G1" H 6250 6940 60  0001 C CNN
F 1 "LOGO" H 6250 7560 60  0001 C CNN
F 2 "linus:linus_logo_negative_big" H 6250 7250 50  0001 C CNN
F 3 "" H 6250 7250 50  0001 C CNN
	1    6250 7250
	1    0    0    -1  
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U14
U 1 1 5C689E0B
P 3950 2500
F 0 "U14" H 4000 2724 50  0000 C CNN
F 1 "NL17SZ125DFT2G" H 4000 2815 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 3950 2500 50  0001 C CNN
F 3 "" H 3950 2500 50  0001 C CNN
	1    3950 2500
	-1   0    0    1   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U16
U 1 1 5C690D90
P 3950 3400
F 0 "U16" H 4000 3624 50  0000 C CNN
F 1 "NL17SZ125DFT2G" H 4000 3715 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 3950 3400 50  0001 C CNN
F 3 "" H 3950 3400 50  0001 C CNN
	1    3950 3400
	-1   0    0    1   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U18
U 1 1 5C698F88
P 3950 4300
F 0 "U18" H 4000 4524 50  0000 C CNN
F 1 "NL17SZ125DFT2G" H 4000 4615 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 3950 4300 50  0001 C CNN
F 3 "" H 3950 4300 50  0001 C CNN
	1    3950 4300
	-1   0    0    1   
$EndComp
Wire Wire Line
	4200 700  5350 700 
Wire Wire Line
	4200 1150 5250 1150
Wire Wire Line
	4200 1600 5150 1600
Wire Wire Line
	5150 1600 5150 4300
Connection ~ 5150 4300
Wire Wire Line
	5050 4100 5050 2050
Wire Wire Line
	5050 2050 4200 2050
Connection ~ 5050 4100
Wire Wire Line
	4200 2500 4950 2500
Wire Wire Line
	4950 2500 4950 3900
Connection ~ 4950 3900
Wire Wire Line
	4850 3700 4850 2950
Wire Wire Line
	4850 2950 4200 2950
Connection ~ 4850 3700
Wire Wire Line
	4750 3500 4750 3400
Wire Wire Line
	4750 3400 4200 3400
Connection ~ 4750 3500
Wire Wire Line
	4650 3850 4200 3850
Connection ~ 4650 3850
Wire Wire Line
	4650 3850 4650 6100
Wire Wire Line
	4550 4300 4200 4300
Connection ~ 4550 4300
Wire Wire Line
	4550 4300 4550 6200
Wire Wire Line
	4450 4750 4200 4750
Connection ~ 4450 4750
Wire Wire Line
	4450 4750 4450 6300
$Comp
L Custom:NL17SZ125DFT2G U16
U 2 1 5C716C6C
P 2200 1400
F 0 "U16" V 1975 1425 50  0000 C CNN
F 1 "NL17SZ125DFT2G" V 2066 1425 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 2200 1400 50  0001 C CNN
F 3 "" H 2200 1400 50  0001 C CNN
	2    2200 1400
	0    1    1    0   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U17
U 2 1 5C71DB3B
P 2200 1700
F 0 "U17" V 1975 1725 50  0000 C CNN
F 1 "NL17SZ125DFT2G" V 2066 1725 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 2200 1700 50  0001 C CNN
F 3 "" H 2200 1700 50  0001 C CNN
	2    2200 1700
	0    1    1    0   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U19
U 2 1 5C72BCAD
P 2200 2300
F 0 "U19" V 1975 2325 50  0000 C CNN
F 1 "NL17SZ125DFT2G" V 2066 2325 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 2200 2300 50  0001 C CNN
F 3 "" H 2200 2300 50  0001 C CNN
	2    2200 2300
	0    1    1    0   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U10
U 2 1 5C733310
P 1150 1100
F 0 "U10" V 925 1125 50  0000 C CNN
F 1 "NL17SZ125DFT2G" V 1016 1125 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 1150 1100 50  0001 C CNN
F 3 "" H 1150 1100 50  0001 C CNN
	2    1150 1100
	0    1    1    0   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U13
U 2 1 5C733325
P 1150 2000
F 0 "U13" V 925 2025 50  0000 C CNN
F 1 "NL17SZ125DFT2G" V 1016 2025 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 1150 2000 50  0001 C CNN
F 3 "" H 1150 2000 50  0001 C CNN
	2    1150 2000
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 1100 1800 1400
Connection ~ 1800 1400
Wire Wire Line
	1800 1400 1800 1700
Connection ~ 1800 1700
Connection ~ 1800 2300
Wire Wire Line
	1800 2300 1800 2400
$Comp
L power:GND #PWR026
U 1 1 5C74139C
P 1800 2400
F 0 "#PWR026" H 1800 2150 50  0001 C CNN
F 1 "GND" H 1805 2227 50  0000 C CNN
F 2 "" H 1800 2400 50  0001 C CNN
F 3 "" H 1800 2400 50  0001 C CNN
	1    1800 2400
	1    0    0    -1  
$EndComp
Connection ~ 2650 1400
Connection ~ 2650 1700
Wire Wire Line
	2650 1700 2650 1400
$Comp
L power:VCC #PWR025
U 1 1 5C748A60
P 2650 750
F 0 "#PWR025" H 2650 600 50  0001 C CNN
F 1 "VCC" H 2667 923 50  0000 C CNN
F 2 "" H 2650 750 50  0001 C CNN
F 3 "" H 2650 750 50  0001 C CNN
	1    2650 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 750  1600 750 
Wire Wire Line
	1600 750  1600 1100
Connection ~ 2650 750 
Connection ~ 1600 1100
Connection ~ 1600 2000
Wire Wire Line
	1600 2000 1600 2300
Wire Wire Line
	750  2400 1800 2400
Connection ~ 1800 2400
Connection ~ 750  2000
Wire Wire Line
	3900 4500 4300 4500
Wire Wire Line
	4300 4500 4300 4050
Wire Wire Line
	4300 600  5700 600 
Wire Wire Line
	3900 450  4300 600 
Connection ~ 4300 600 
Wire Wire Line
	1800 1700 1800 2000
Wire Wire Line
	2650 1700 2650 2000
Wire Wire Line
	2650 750  2650 1100
Wire Wire Line
	1600 1100 1600 1400
Wire Wire Line
	750  2000 750  2300
Wire Wire Line
	750  1100 750  1400
$Comp
L Custom:NL17SZ125DFT2G U12
U 1 1 5C6AF47C
P 3950 1600
F 0 "U12" H 4000 1824 50  0000 C CNN
F 1 "NL17SZ125DFT2G" H 4000 1915 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 3950 1600 50  0001 C CNN
F 3 "" H 3950 1600 50  0001 C CNN
	1    3950 1600
	-1   0    0    1   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U10
U 1 1 5C6828AA
P 3950 700
F 0 "U10" H 4000 924 50  0000 C CNN
F 1 "NL17SZ125DFT2G" H 4000 1015 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 3950 700 50  0001 C CNN
F 3 "" H 3950 700 50  0001 C CNN
	1    3950 700 
	-1   0    0    1   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U19
U 1 1 5C694F6A
P 3950 4750
F 0 "U19" H 4000 4974 50  0000 C CNN
F 1 "NL17SZ125DFT2G" H 4000 5065 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 3950 4750 50  0001 C CNN
F 3 "" H 3950 4750 50  0001 C CNN
	1    3950 4750
	-1   0    0    1   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U17
U 1 1 5C69D8F2
P 3950 3850
F 0 "U17" H 4000 4074 50  0000 C CNN
F 1 "NL17SZ125DFT2G" H 4000 4165 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 3950 3850 50  0001 C CNN
F 3 "" H 3950 3850 50  0001 C CNN
	1    3950 3850
	-1   0    0    1   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U15
U 1 1 5C6A627A
P 3950 2950
F 0 "U15" H 4000 3174 50  0000 C CNN
F 1 "NL17SZ125DFT2G" H 4000 3265 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 3950 2950 50  0001 C CNN
F 3 "" H 3950 2950 50  0001 C CNN
	1    3950 2950
	-1   0    0    1   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U13
U 1 1 5C6AEDD2
P 3950 2050
F 0 "U13" H 4000 2274 50  0000 C CNN
F 1 "NL17SZ125DFT2G" H 4000 2365 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 3950 2050 50  0001 C CNN
F 3 "" H 3950 2050 50  0001 C CNN
	1    3950 2050
	-1   0    0    1   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U11
U 1 1 5C6B77D1
P 3950 1150
F 0 "U11" H 4000 1374 50  0000 C CNN
F 1 "NL17SZ125DFT2G" H 4000 1465 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 3950 1150 50  0001 C CNN
F 3 "" H 3950 1150 50  0001 C CNN
	1    3950 1150
	-1   0    0    1   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U18
U 2 1 5C6C1B88
P 2200 2000
F 0 "U18" V 1975 2025 50  0000 C CNN
F 1 "NL17SZ125DFT2G" V 2066 2025 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 2200 2000 50  0001 C CNN
F 3 "" H 2200 2000 50  0001 C CNN
	2    2200 2000
	0    1    1    0   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U15
U 2 1 5C6CA510
P 2200 1100
F 0 "U15" V 1975 1125 50  0000 C CNN
F 1 "NL17SZ125DFT2G" V 2066 1125 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 2200 1100 50  0001 C CNN
F 3 "" H 2200 1100 50  0001 C CNN
	2    2200 1100
	0    1    1    0   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U11
U 2 1 5C6D2EA6
P 1150 1400
F 0 "U11" V 925 1425 50  0000 C CNN
F 1 "NL17SZ125DFT2G" V 1016 1425 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 1150 1400 50  0001 C CNN
F 3 "" H 1150 1400 50  0001 C CNN
	2    1150 1400
	0    1    1    0   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U12
U 2 1 5C6DB830
P 1150 1700
F 0 "U12" V 925 1725 50  0000 C CNN
F 1 "NL17SZ125DFT2G" V 1016 1725 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 1150 1700 50  0001 C CNN
F 3 "" H 1150 1700 50  0001 C CNN
	2    1150 1700
	0    1    1    0   
$EndComp
$Comp
L Custom:NL17SZ125DFT2G U14
U 2 1 5C6E41C0
P 1150 2300
F 0 "U14" V 925 2325 50  0000 C CNN
F 1 "NL17SZ125DFT2G" V 1016 2325 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 1150 2300 50  0001 C CNN
F 3 "" H 1150 2300 50  0001 C CNN
	2    1150 2300
	0    1    1    0   
$EndComp
Connection ~ 750  1700
Wire Wire Line
	750  1700 750  2000
Connection ~ 750  2300
Wire Wire Line
	750  2300 750  2400
Connection ~ 1600 1700
Wire Wire Line
	1600 1700 1600 2000
Connection ~ 1800 2000
Wire Wire Line
	1800 2000 1800 2300
Connection ~ 2650 1100
Wire Wire Line
	2650 1100 2650 1400
Connection ~ 2650 2000
Wire Wire Line
	2650 2000 2650 2300
Wire Wire Line
	3900 4050 4300 4050
Connection ~ 4300 4050
Wire Wire Line
	4300 4050 4300 3600
Wire Wire Line
	3900 3600 4300 3600
Connection ~ 4300 3600
Wire Wire Line
	4300 3600 4300 3150
Wire Wire Line
	3900 3150 4300 3150
Connection ~ 4300 3150
Wire Wire Line
	4300 3150 4300 2700
Wire Wire Line
	3900 2700 4300 2700
Connection ~ 4300 2700
Wire Wire Line
	3900 900  4300 900 
Connection ~ 4300 900 
Wire Wire Line
	4300 900  4300 600 
Wire Wire Line
	3900 1350 4300 1350
Connection ~ 4300 1350
Wire Wire Line
	4300 1350 4300 900 
Wire Wire Line
	3900 1800 4300 1800
Connection ~ 4300 1800
Wire Wire Line
	4300 1800 4300 1350
Wire Wire Line
	3900 2250 4300 2250
Wire Wire Line
	4300 1800 4300 2250
Connection ~ 4300 2250
Wire Wire Line
	4300 2250 4300 2700
$Comp
L Connector:TestPoint TP89
U 1 1 5C7537CC
P 5700 600
F 0 "TP89" V 5648 788 50  0000 L CNN
F 1 "~ALU_OUT" V 5746 788 50  0000 L CNN
F 2 "Custom:TestPoint_Pad_2.0x2.0mm_no_ref" H 5900 600 50  0001 C CNN
F 3 "~" H 5900 600 50  0001 C CNN
	1    5700 600 
	0    1    1    0   
$EndComp
Text Label 5550 600  0    50   ~ 0
~EO
Connection ~ 750  1400
Wire Wire Line
	750  1400 750  1700
Connection ~ 1600 1400
Wire Wire Line
	1600 1400 1600 1700
$Comp
L Mechanical:MountingHole H16
U 1 1 5C9E98C9
P 1500 7550
F 0 "H16" H 1600 7596 50  0000 L CNN
F 1 "MountingHole" H 1600 7505 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1500 7550 50  0001 C CNN
F 3 "~" H 1500 7550 50  0001 C CNN
	1    1500 7550
	1    0    0    -1  
$EndComp
$EndSCHEMATC
