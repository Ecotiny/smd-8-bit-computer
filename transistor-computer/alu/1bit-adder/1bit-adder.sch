EESchema Schematic File Version 4
LIBS:1bit-adder-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1500 1000 550  400 
U 5C450B91
F0 "xor" 50
F1 "xor.sch" 50
F2 "A" I L 1500 1100 50 
F3 "B" I L 1500 1300 50 
F4 "Q" O R 2050 1200 50 
$EndSheet
Wire Wire Line
	2450 1600 2250 1600
Wire Wire Line
	2250 1600 2250 1200
Wire Wire Line
	2250 1200 2150 1200
Text Label 650  1800 0    50   ~ 0
CarryIn
Text Label 700  1300 0    50   ~ 0
B
Text Label 700  1100 0    50   ~ 0
A
Text Label 4400 1700 0    50   ~ 0
sum
$Sheet
S 3500 2900 500  300 
U 5C4520E6
F0 "or" 50
F1 "or.sch" 50
F2 "Q" O R 4000 3050 50 
F3 "A" I L 3500 2950 50 
F4 "B" I L 3500 3150 50 
$EndSheet
$Sheet
S 2300 2550 600  300 
U 5C4524F7
F0 "and" 50
F1 "and.sch" 50
F2 "A" I L 2300 2600 50 
F3 "B" I L 2300 2800 50 
F4 "Q" O R 2900 2700 50 
$EndSheet
$Sheet
S 2450 1500 550  400 
U 5C451DE1
F0 "sheet5C451DDC" 50
F1 "xor.sch" 50
F2 "A" I L 2450 1600 50 
F3 "B" I L 2450 1800 50 
F4 "Q" O R 3000 1700 50 
$EndSheet
$Sheet
S 2300 3250 600  300 
U 5C4528E1
F0 "sheet5C4528DC" 50
F1 "and.sch" 50
F2 "A" I L 2300 3300 50 
F3 "B" I L 2300 3500 50 
F4 "Q" O R 2900 3400 50 
$EndSheet
Wire Wire Line
	2900 3400 3400 3400
Wire Wire Line
	3400 3400 3400 3150
Wire Wire Line
	3400 3150 3500 3150
Wire Wire Line
	3500 2950 3400 2950
Wire Wire Line
	3400 2950 3400 2700
Wire Wire Line
	3400 2700 2900 2700
Wire Wire Line
	600  1800 2200 1800
Wire Wire Line
	600  1300 1500 1300
Wire Wire Line
	600  1100 1350 1100
Wire Wire Line
	2300 2600 2200 2600
Wire Wire Line
	2200 2600 2200 1800
Connection ~ 2200 1800
Wire Wire Line
	2200 1800 2450 1800
Wire Wire Line
	2300 2800 2150 2800
Wire Wire Line
	2150 2800 2150 1200
Connection ~ 2150 1200
Wire Wire Line
	2150 1200 2050 1200
Wire Wire Line
	2300 3300 1400 3300
Wire Wire Line
	1400 3300 1400 1100
Connection ~ 1400 1100
Wire Wire Line
	1400 1100 1500 1100
Wire Wire Line
	1350 1100 1350 3500
Wire Wire Line
	1350 3500 2300 3500
Connection ~ 1350 1100
Wire Wire Line
	1350 1100 1400 1100
Wire Wire Line
	3000 1700 4600 1700
Wire Wire Line
	4000 3050 4600 3050
Text Label 4550 3050 2    50   ~ 0
cOut
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5C455151
P 750 4350
F 0 "H1" H 650 4308 50  0000 R CNN
F 1 "MountingHole_Pad" H 650 4399 50  0000 R CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 750 4350 50  0001 C CNN
F 3 "~" H 750 4350 50  0001 C CNN
	1    750  4350
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5C45553C
P 2700 4150
F 0 "H3" H 2800 4201 50  0000 L CNN
F 1 "MountingHole_Pad" H 2800 4110 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 2700 4150 50  0001 C CNN
F 3 "~" H 2700 4150 50  0001 C CNN
	1    2700 4150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5C455909
P 600 1700
F 0 "H4" H 700 1751 50  0000 L CNN
F 1 "MountingHole_Pad" H 700 1850 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 600 1700 50  0001 C CNN
F 3 "~" H 600 1700 50  0001 C CNN
	1    600  1700
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0101
U 1 1 5C455D15
P 750 4250
F 0 "#PWR0101" H 750 4100 50  0001 C CNN
F 1 "VCC" H 767 4423 50  0000 C CNN
F 2 "" H 750 4250 50  0001 C CNN
F 3 "" H 750 4250 50  0001 C CNN
	1    750  4250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5C456503
P 1650 4350
F 0 "H2" H 1550 4308 50  0000 R CNN
F 1 "MountingHole_Pad" H 1550 4399 50  0000 R CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1650 4350 50  0001 C CNN
F 3 "~" H 1650 4350 50  0001 C CNN
	1    1650 4350
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 5C45650A
P 1650 4250
F 0 "#PWR0102" H 1650 4100 50  0001 C CNN
F 1 "VCC" H 1667 4423 50  0000 C CNN
F 2 "" H 1650 4250 50  0001 C CNN
F 3 "" H 1650 4250 50  0001 C CNN
	1    1650 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5C4568EB
P 2700 4250
F 0 "#PWR0103" H 2700 4000 50  0001 C CNN
F 1 "GND" H 2705 4077 50  0000 C CNN
F 2 "" H 2700 4250 50  0001 C CNN
F 3 "" H 2700 4250 50  0001 C CNN
	1    2700 4250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H5
U 1 1 5C4582F9
P 4700 3050
F 0 "H5" V 4654 3200 50  0000 L CNN
F 1 "MountingHole_Pad" V 4745 3200 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 4700 3050 50  0001 C CNN
F 3 "~" H 4700 3050 50  0001 C CNN
	1    4700 3050
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5C4593FC
P 600 1100
F 0 "TP1" H 658 1220 50  0000 L CNN
F 1 "TestPoint" H 658 1129 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 800 1100 50  0001 C CNN
F 3 "~" H 800 1100 50  0001 C CNN
	1    600  1100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5C45944E
P 600 1300
F 0 "TP2" H 658 1420 50  0000 L CNN
F 1 "TestPoint" H 658 1329 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 800 1300 50  0001 C CNN
F 3 "~" H 800 1300 50  0001 C CNN
	1    600  1300
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5C4595E1
P 4600 1700
F 0 "TP3" H 4658 1820 50  0000 L CNN
F 1 "TestPoint" H 4658 1729 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 4800 1700 50  0001 C CNN
F 3 "~" H 4800 1700 50  0001 C CNN
	1    4600 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5C4598BF
P 4600 1850
F 0 "D1" V 4638 1733 50  0000 R CNN
F 1 "LED" V 4547 1733 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4600 1850 50  0001 C CNN
F 3 "~" H 4600 1850 50  0001 C CNN
	1    4600 1850
	0    -1   -1   0   
$EndComp
Connection ~ 4600 1700
$Comp
L Device:R R1
U 1 1 5C459987
P 4600 2150
F 0 "R1" H 4670 2196 50  0000 L CNN
F 1 "220" H 4670 2105 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4530 2150 50  0001 C CNN
F 3 "~" H 4600 2150 50  0001 C CNN
	1    4600 2150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5C4599E1
P 4600 2300
F 0 "#PWR0104" H 4600 2050 50  0001 C CNN
F 1 "GND" H 4605 2127 50  0000 C CNN
F 2 "" H 4600 2300 50  0001 C CNN
F 3 "" H 4600 2300 50  0001 C CNN
	1    4600 2300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
