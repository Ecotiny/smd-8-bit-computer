EESchema Schematic File Version 4
LIBS:nand-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 5C1DCA9F
P 2650 1600
AR Path="/5C1DCA9F" Ref="R?"  Part="1" 
AR Path="/5C1DC876/5C1DCA9F" Ref="R1"  Part="1" 
AR Path="/5C1DCE57/5C1DCA9F" Ref="R3"  Part="1" 
AR Path="/5C1DCF44/5C1DCA9F" Ref="R5"  Part="1" 
AR Path="/5C1DCF49/5C1DCA9F" Ref="R7"  Part="1" 
AR Path="/5C1DD0C0/5C1DCA9F" Ref="R?"  Part="1" 
AR Path="/5C1DD0C5/5C1DCA9F" Ref="R9"  Part="1" 
F 0 "R1" H 2720 1646 50  0000 L CNN
F 1 "4.7K" H 2720 1555 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2580 1600 50  0001 C CNN
F 3 "~" H 2650 1600 50  0001 C CNN
	1    2650 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 5C1DCAA6
P 2650 2050
AR Path="/5C1DCAA6" Ref="Q?"  Part="1" 
AR Path="/5C1DC876/5C1DCAA6" Ref="Q1"  Part="1" 
AR Path="/5C1DCE57/5C1DCAA6" Ref="Q4"  Part="1" 
AR Path="/5C1DCF44/5C1DCAA6" Ref="Q7"  Part="1" 
AR Path="/5C1DCF49/5C1DCAA6" Ref="Q10"  Part="1" 
AR Path="/5C1DD0C0/5C1DCAA6" Ref="Q?"  Part="1" 
AR Path="/5C1DD0C5/5C1DCAA6" Ref="Q13"  Part="1" 
F 0 "Q1" V 2886 2050 50  0000 C CNN
F 1 "Q_NPN_BEC" V 2977 2050 50  0000 C CNN
F 2 "Custom:SOT-23" H 2850 2150 50  0001 C CNN
F 3 "~" H 2650 2050 50  0001 C CNN
	1    2650 2050
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 5C1DCAAD
P 3300 2150
AR Path="/5C1DCAAD" Ref="Q?"  Part="1" 
AR Path="/5C1DC876/5C1DCAAD" Ref="Q3"  Part="1" 
AR Path="/5C1DCE57/5C1DCAAD" Ref="Q6"  Part="1" 
AR Path="/5C1DCF44/5C1DCAAD" Ref="Q9"  Part="1" 
AR Path="/5C1DCF49/5C1DCAAD" Ref="Q12"  Part="1" 
AR Path="/5C1DD0C0/5C1DCAAD" Ref="Q?"  Part="1" 
AR Path="/5C1DD0C5/5C1DCAAD" Ref="Q15"  Part="1" 
F 0 "Q3" H 3491 2196 50  0000 L CNN
F 1 "Q_NPN_BEC" H 3491 2105 50  0000 L CNN
F 2 "Custom:SOT-23" H 3500 2250 50  0001 C CNN
F 3 "~" H 3300 2150 50  0001 C CNN
	1    3300 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5C1DCAB4
P 3400 1600
AR Path="/5C1DCAB4" Ref="R?"  Part="1" 
AR Path="/5C1DC876/5C1DCAB4" Ref="R2"  Part="1" 
AR Path="/5C1DCE57/5C1DCAB4" Ref="R4"  Part="1" 
AR Path="/5C1DCF44/5C1DCAB4" Ref="R6"  Part="1" 
AR Path="/5C1DCF49/5C1DCAB4" Ref="R8"  Part="1" 
AR Path="/5C1DD0C0/5C1DCAB4" Ref="R?"  Part="1" 
AR Path="/5C1DD0C5/5C1DCAB4" Ref="R10"  Part="1" 
F 0 "R2" H 3470 1646 50  0000 L CNN
F 1 "1K" H 3470 1555 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3330 1600 50  0001 C CNN
F 3 "~" H 3400 1600 50  0001 C CNN
	1    3400 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 1450 3400 1300
Wire Wire Line
	3400 1300 2650 1300
Wire Wire Line
	2650 1450 2650 1300
Wire Wire Line
	2650 1850 2650 1800
Wire Wire Line
	3400 1750 3400 1850
Wire Wire Line
	2650 1800 2900 1800
Wire Wire Line
	2900 1800 2900 2400
Connection ~ 2650 1800
Wire Wire Line
	2650 1800 2650 1750
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 5C1DCAC4
P 2900 2600
AR Path="/5C1DCAC4" Ref="Q?"  Part="1" 
AR Path="/5C1DC876/5C1DCAC4" Ref="Q2"  Part="1" 
AR Path="/5C1DCE57/5C1DCAC4" Ref="Q5"  Part="1" 
AR Path="/5C1DCF44/5C1DCAC4" Ref="Q8"  Part="1" 
AR Path="/5C1DCF49/5C1DCAC4" Ref="Q11"  Part="1" 
AR Path="/5C1DD0C0/5C1DCAC4" Ref="Q?"  Part="1" 
AR Path="/5C1DD0C5/5C1DCAC4" Ref="Q14"  Part="1" 
F 0 "Q2" V 3136 2600 50  0000 C CNN
F 1 "Q_NPN_BEC" V 3227 2600 50  0000 C CNN
F 2 "Custom:SOT-23" H 3100 2700 50  0001 C CNN
F 3 "~" H 2900 2600 50  0001 C CNN
	1    2900 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	2850 2150 3100 2150
Wire Wire Line
	3100 2700 3100 2150
Connection ~ 3100 2150
Wire Wire Line
	3400 2350 3400 3450
Text Label 2300 2700 0    50   ~ 0
B
Text Label 2300 2150 0    50   ~ 0
A
Wire Wire Line
	3400 1850 4100 1850
Wire Wire Line
	4100 1850 4100 3050
Connection ~ 3400 1850
Wire Wire Line
	3400 1850 3400 1950
Text Label 2300 3050 0    50   ~ 0
Q
Wire Wire Line
	2200 2150 2450 2150
Connection ~ 2650 1300
Wire Wire Line
	2200 1300 2650 1300
Wire Wire Line
	4100 3050 2200 3050
Wire Wire Line
	2700 2700 2200 2700
$Comp
L power:GND #PWR04
U 1 1 5C1DCC7C
P 3400 3450
AR Path="/5C1DC876/5C1DCC7C" Ref="#PWR04"  Part="1" 
AR Path="/5C1DCE57/5C1DCC7C" Ref="#PWR06"  Part="1" 
AR Path="/5C1DCF44/5C1DCC7C" Ref="#PWR08"  Part="1" 
AR Path="/5C1DCF49/5C1DCC7C" Ref="#PWR010"  Part="1" 
AR Path="/5C1DD0C0/5C1DCC7C" Ref="#PWR?"  Part="1" 
AR Path="/5C1DD0C5/5C1DCC7C" Ref="#PWR012"  Part="1" 
F 0 "#PWR04" H 3400 3200 50  0001 C CNN
F 1 "GND" H 3405 3277 50  0000 C CNN
F 2 "" H 3400 3450 50  0001 C CNN
F 3 "" H 3400 3450 50  0001 C CNN
	1    3400 3450
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR03
U 1 1 5C1DCCF5
P 2200 1300
AR Path="/5C1DC876/5C1DCCF5" Ref="#PWR03"  Part="1" 
AR Path="/5C1DCE57/5C1DCCF5" Ref="#PWR05"  Part="1" 
AR Path="/5C1DCF44/5C1DCCF5" Ref="#PWR07"  Part="1" 
AR Path="/5C1DCF49/5C1DCCF5" Ref="#PWR09"  Part="1" 
AR Path="/5C1DD0C0/5C1DCCF5" Ref="#PWR?"  Part="1" 
AR Path="/5C1DD0C5/5C1DCCF5" Ref="#PWR011"  Part="1" 
F 0 "#PWR03" H 2200 1150 50  0001 C CNN
F 1 "VCC" H 2217 1473 50  0000 C CNN
F 2 "" H 2200 1300 50  0001 C CNN
F 3 "" H 2200 1300 50  0001 C CNN
	1    2200 1300
	1    0    0    -1  
$EndComp
Text HLabel 2200 2150 0    50   Input ~ 0
A
Text HLabel 2200 2700 0    50   Input ~ 0
B
Text HLabel 2200 3050 0    50   Output ~ 0
Q
$EndSCHEMATC
