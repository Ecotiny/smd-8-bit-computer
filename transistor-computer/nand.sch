EESchema Schematic File Version 4
LIBS:nand-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R1
U 1 1 5C1D95DC
P 2200 1600
F 0 "R1" H 2270 1646 50  0000 L CNN
F 1 "4.7K" H 2270 1555 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2130 1600 50  0001 C CNN
F 3 "~" H 2200 1600 50  0001 C CNN
	1    2200 1600
	-1   0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BEC Q1
U 1 1 5C1D961C
P 2200 2050
F 0 "Q1" V 2436 2050 50  0000 C CNN
F 1 "Q_NPN_BEC" V 2527 2050 50  0000 C CNN
F 2 "Custom:SOT-23" H 2400 2150 50  0001 C CNN
F 3 "~" H 2200 2050 50  0001 C CNN
	1    2200 2050
	0    -1   1    0   
$EndComp
$Comp
L Device:Q_NPN_BEC Q3
U 1 1 5C1D96EE
P 1550 2150
F 0 "Q3" H 1741 2196 50  0000 L CNN
F 1 "Q_NPN_BEC" H 1741 2105 50  0000 L CNN
F 2 "Custom:SOT-23" H 1750 2250 50  0001 C CNN
F 3 "~" H 1550 2150 50  0001 C CNN
	1    1550 2150
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5C1D9738
P 1450 1600
F 0 "R2" H 1520 1646 50  0000 L CNN
F 1 "1K" H 1520 1555 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1380 1600 50  0001 C CNN
F 3 "~" H 1450 1600 50  0001 C CNN
	1    1450 1600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1450 1450 1450 1300
Wire Wire Line
	1450 1300 2200 1300
Wire Wire Line
	2200 1450 2200 1300
Connection ~ 2200 1300
Wire Wire Line
	2200 1850 2200 1800
Wire Wire Line
	1450 1750 1450 1850
Wire Wire Line
	2200 1800 1950 1800
Wire Wire Line
	1950 1800 1950 2400
Connection ~ 2200 1800
Wire Wire Line
	2200 1800 2200 1750
$Comp
L Device:Q_NPN_BEC Q2
U 1 1 5C1D9897
P 1950 2600
F 0 "Q2" V 2186 2600 50  0000 C CNN
F 1 "Q_NPN_BEC" V 2277 2600 50  0000 C CNN
F 2 "Custom:SOT-23" H 2150 2700 50  0001 C CNN
F 3 "~" H 1950 2600 50  0001 C CNN
	1    1950 2600
	0    -1   1    0   
$EndComp
Wire Wire Line
	2000 2150 1750 2150
Wire Wire Line
	1750 2700 1750 2150
Connection ~ 1750 2150
Wire Wire Line
	1450 2350 1450 3450
Text Label 2550 3450 2    50   ~ 0
GND
Text Label 2550 2700 2    50   ~ 0
B
Text Label 2550 2150 2    50   ~ 0
A
Text Label 2550 1300 2    50   ~ 0
VCC
$Comp
L Custom:DF37C-10DS-0-4V-51(MALE) J1
U 1 1 5C1D9DE4
P 4300 2300
F 0 "J1" H 4314 1935 50  0000 C CNN
F 1 "DF37C-10DS-0-4V-51(MALE)" H 4314 2026 50  0000 C CNN
F 2 "Custom:DF37C-10DP-0.4V-51" H 4300 2300 50  0001 C CNN
F 3 "" H 4300 2300 50  0001 C CNN
	1    4300 2300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1450 1850 750  1850
Wire Wire Line
	750  1850 750  3050
Connection ~ 1450 1850
Wire Wire Line
	1450 1850 1450 1950
Text Label 2550 3050 2    50   ~ 0
Q
Wire Wire Line
	3700 1300 3700 2250
Wire Wire Line
	3700 2250 4100 2250
Wire Wire Line
	3700 1300 2200 1300
Wire Wire Line
	3550 2150 3550 2300
Wire Wire Line
	3550 2300 4100 2300
Wire Wire Line
	3550 2150 2400 2150
Wire Wire Line
	4050 2700 4050 2350
Wire Wire Line
	4050 2350 4100 2350
Wire Wire Line
	4050 2700 2150 2700
Wire Wire Line
	4000 3050 4000 2400
Wire Wire Line
	4000 2400 4100 2400
Wire Wire Line
	4000 3050 750  3050
Wire Wire Line
	4500 3450 4500 2450
Wire Wire Line
	4500 3450 1450 3450
$Comp
L power:GND #PWR02
U 1 1 5C1ECF69
P 1450 3450
F 0 "#PWR02" H 1450 3200 50  0001 C CNN
F 1 "GND" H 1455 3277 50  0000 C CNN
F 2 "" H 1450 3450 50  0001 C CNN
F 3 "" H 1450 3450 50  0001 C CNN
	1    1450 3450
	1    0    0    -1  
$EndComp
Connection ~ 1450 3450
$Comp
L power:VCC #PWR01
U 1 1 5C1ECFCA
P 1450 1300
F 0 "#PWR01" H 1450 1150 50  0001 C CNN
F 1 "VCC" H 1467 1473 50  0000 C CNN
F 2 "" H 1450 1300 50  0001 C CNN
F 3 "" H 1450 1300 50  0001 C CNN
	1    1450 1300
	1    0    0    -1  
$EndComp
Connection ~ 1450 1300
$Comp
L Device:R R5
U 1 1 5C1ED2B1
P 7850 1100
F 0 "R5" H 7920 1146 50  0000 L CNN
F 1 "4.7K" H 7920 1055 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 1100 50  0001 C CNN
F 3 "~" H 7850 1100 50  0001 C CNN
	1    7850 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BEC Q6
U 1 1 5C1ED2B8
P 7850 1550
F 0 "Q6" V 8086 1550 50  0000 C CNN
F 1 "Q_NPN_BEC" V 8177 1550 50  0000 C CNN
F 2 "Custom:SOT-23" H 8050 1650 50  0001 C CNN
F 3 "~" H 7850 1550 50  0001 C CNN
	1    7850 1550
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NPN_BEC Q8
U 1 1 5C1ED2BF
P 8500 1650
F 0 "Q8" H 8691 1696 50  0000 L CNN
F 1 "Q_NPN_BEC" H 8691 1605 50  0000 L CNN
F 2 "Custom:SOT-23" H 8700 1750 50  0001 C CNN
F 3 "~" H 8500 1650 50  0001 C CNN
	1    8500 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5C1ED2C6
P 8600 1100
F 0 "R6" H 8670 1146 50  0000 L CNN
F 1 "1K" H 8670 1055 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 8530 1100 50  0001 C CNN
F 3 "~" H 8600 1100 50  0001 C CNN
	1    8600 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 950  8600 800 
Wire Wire Line
	8600 800  7850 800 
Wire Wire Line
	7850 950  7850 800 
Wire Wire Line
	7850 1350 7850 1300
Wire Wire Line
	8600 1250 8600 1350
Wire Wire Line
	7850 1300 8100 1300
Wire Wire Line
	8100 1300 8100 1900
Connection ~ 7850 1300
Wire Wire Line
	7850 1300 7850 1250
$Comp
L Device:Q_NPN_BEC Q7
U 1 1 5C1ED2D7
P 8100 2100
F 0 "Q7" V 8336 2100 50  0000 C CNN
F 1 "Q_NPN_BEC" V 8427 2100 50  0000 C CNN
F 2 "Custom:SOT-23" H 8300 2200 50  0001 C CNN
F 3 "~" H 8100 2100 50  0001 C CNN
	1    8100 2100
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 1650 8300 1650
Wire Wire Line
	8300 2200 8300 1650
Connection ~ 8300 1650
Wire Wire Line
	8600 1850 8600 2950
Text Label 7500 2200 0    50   ~ 0
B1
Text Label 7500 1650 0    50   ~ 0
A1
Wire Wire Line
	8600 1350 9300 1350
Wire Wire Line
	9300 1350 9300 2550
Connection ~ 8600 1350
Wire Wire Line
	8600 1350 8600 1450
Text Label 7500 2550 0    50   ~ 0
Q1
$Comp
L power:GND #PWR07
U 1 1 5C1ED2F0
P 8600 2950
F 0 "#PWR07" H 8600 2700 50  0001 C CNN
F 1 "GND" H 8605 2777 50  0000 C CNN
F 2 "" H 8600 2950 50  0001 C CNN
F 3 "" H 8600 2950 50  0001 C CNN
	1    8600 2950
	-1   0    0    -1  
$EndComp
$Comp
L power:VCC #PWR06
U 1 1 5C1ED2F7
P 8600 800
F 0 "#PWR06" H 8600 650 50  0001 C CNN
F 1 "VCC" H 8617 973 50  0000 C CNN
F 2 "" H 8600 800 50  0001 C CNN
F 3 "" H 8600 800 50  0001 C CNN
	1    8600 800 
	-1   0    0    -1  
$EndComp
Connection ~ 8600 800 
Wire Wire Line
	4500 1650 4500 2250
Wire Wire Line
	4500 1650 7650 1650
Wire Wire Line
	4600 2200 4600 2300
Wire Wire Line
	4600 2300 4500 2300
Wire Wire Line
	4600 2200 7900 2200
Wire Wire Line
	4750 2550 4750 2350
Wire Wire Line
	4750 2350 4500 2350
Wire Wire Line
	4750 2550 9300 2550
$Comp
L Device:Q_NPN_BEC Q5
U 1 1 5C1F27A6
P 4600 4250
F 0 "Q5" V 4836 4250 50  0000 C CNN
F 1 "Q_NPN_BEC" V 4927 4250 50  0000 C CNN
F 2 "Custom:SOT-23" H 4800 4350 50  0001 C CNN
F 3 "~" H 4600 4250 50  0001 C CNN
	1    4600 4250
	0    -1   1    0   
$EndComp
$Comp
L Device:Q_NPN_BEC Q4
U 1 1 5C1F27AD
P 3950 4350
F 0 "Q4" H 4141 4396 50  0000 L CNN
F 1 "Q_NPN_BEC" H 4141 4305 50  0000 L CNN
F 2 "Custom:SOT-23" H 4150 4450 50  0001 C CNN
F 3 "~" H 3950 4350 50  0001 C CNN
	1    3950 4350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4400 4350 4150 4350
$Comp
L Device:R R4
U 1 1 5C1F48EA
P 4600 3900
F 0 "R4" H 4670 3946 50  0000 L CNN
F 1 "4.7K" H 4670 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4530 3900 50  0001 C CNN
F 3 "~" H 4600 3900 50  0001 C CNN
	1    4600 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5C1F48F1
P 3850 3850
F 0 "R3" H 3920 3896 50  0000 L CNN
F 1 "1K" H 3920 3805 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3780 3850 50  0001 C CNN
F 3 "~" H 3850 3850 50  0001 C CNN
	1    3850 3850
	-1   0    0    -1  
$EndComp
$Comp
L power:VCC #PWR03
U 1 1 5C1FD9BF
P 3850 3700
F 0 "#PWR03" H 3850 3550 50  0001 C CNN
F 1 "VCC" H 3867 3873 50  0000 C CNN
F 2 "" H 3850 3700 50  0001 C CNN
F 3 "" H 3850 3700 50  0001 C CNN
	1    3850 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5C1FDA23
P 3850 4550
F 0 "#PWR04" H 3850 4300 50  0001 C CNN
F 1 "GND" H 3855 4377 50  0000 C CNN
F 2 "" H 3850 4550 50  0001 C CNN
F 3 "" H 3850 4550 50  0001 C CNN
	1    3850 4550
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 5C1FDADC
P 4600 3750
F 0 "#PWR05" H 4600 3600 50  0001 C CNN
F 1 "VCC" H 4617 3923 50  0000 C CNN
F 2 "" H 4600 3750 50  0001 C CNN
F 3 "" H 4600 3750 50  0001 C CNN
	1    4600 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 4000 3850 4050
Wire Wire Line
	4800 4350 5000 4350
Wire Wire Line
	5000 4350 5000 2850
Wire Wire Line
	5000 2850 4600 2850
Wire Wire Line
	4600 2850 4600 2400
Wire Wire Line
	4600 2400 4500 2400
Wire Wire Line
	3850 4050 4050 4050
Wire Wire Line
	4050 4050 4050 3200
Wire Wire Line
	4050 3200 3550 3200
Wire Wire Line
	3550 3200 3550 2450
Wire Wire Line
	3550 2450 4100 2450
Connection ~ 3850 4050
Wire Wire Line
	3850 4050 3850 4150
$EndSCHEMATC
