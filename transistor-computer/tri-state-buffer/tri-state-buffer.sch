EESchema Schematic File Version 4
LIBS:tri-state-buffer-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5C838E49
P 4100 3300
F 0 "H1" H 4200 3351 50  0000 L CNN
F 1 "GND" H 4200 3260 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 4100 3300 50  0001 C CNN
F 3 "~" H 4100 3300 50  0001 C CNN
	1    4100 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5C838F6E
P 4100 3400
F 0 "#PWR0101" H 4100 3150 50  0001 C CNN
F 1 "GND" H 4105 3227 50  0000 C CNN
F 2 "" H 4100 3400 50  0001 C CNN
F 3 "" H 4100 3400 50  0001 C CNN
	1    4100 3400
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 5C838FD5
P 4500 3100
F 0 "#PWR0102" H 4500 2950 50  0001 C CNN
F 1 "VCC" H 4517 3273 50  0000 C CNN
F 2 "" H 4500 3100 50  0001 C CNN
F 3 "" H 4500 3100 50  0001 C CNN
	1    4500 3100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5C8390CA
P 4500 3200
F 0 "H2" H 4400 3158 50  0000 R CNN
F 1 "VCC" H 4400 3249 50  0000 R CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 4500 3200 50  0001 C CNN
F 3 "~" H 4500 3200 50  0001 C CNN
	1    4500 3200
	-1   0    0    1   
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5C83A00B
P 3550 4100
F 0 "TP1" V 3750 4200 50  0000 C CNN
F 1 "A" V 3650 4200 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 3750 4100 50  0001 C CNN
F 3 "~" H 3750 4100 50  0001 C CNN
	1    3550 4100
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5C83A894
P 5550 4300
F 0 "TP2" V 5504 4488 50  0000 L CNN
F 1 "Q" V 5595 4488 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 5750 4300 50  0001 C CNN
F 3 "~" H 5750 4300 50  0001 C CNN
	1    5550 4300
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5C83AFBC
P 3450 4800
F 0 "H3" V 3400 5000 50  0000 C CNN
F 1 "ENABLE" V 3500 5100 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 3450 4800 50  0001 C CNN
F 3 "~" H 3450 4800 50  0001 C CNN
	1    3450 4800
	0    -1   1    0   
$EndComp
$Comp
L Device:Q_NPN_BEC Q3
U 1 1 5C6894D4
P 4100 4800
F 0 "Q3" H 4291 4846 50  0000 L CNN
F 1 "Q_NPN_BEC" H 4291 4755 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 4300 4900 50  0001 C CNN
F 3 "~" H 4100 4800 50  0001 C CNN
	1    4100 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BEC Q2
U 1 1 5C68954F
P 4400 4450
F 0 "Q2" H 4591 4496 50  0000 L CNN
F 1 "Q_NPN_BEC" H 4591 4405 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 4600 4550 50  0001 C CNN
F 3 "~" H 4400 4450 50  0001 C CNN
	1    4400 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BEC Q1
U 1 1 5C68970E
P 4700 4100
F 0 "Q1" H 4891 4146 50  0000 L CNN
F 1 "Q_NPN_BEC" H 4891 4055 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 4900 4200 50  0001 C CNN
F 3 "~" H 4700 4100 50  0001 C CNN
	1    4700 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 5000 4500 5000
Wire Wire Line
	4500 5000 4500 4650
$Comp
L power:GND #PWR03
U 1 1 5C689A6A
P 4500 5000
F 0 "#PWR03" H 4500 4750 50  0001 C CNN
F 1 "GND" H 4505 4827 50  0000 C CNN
F 2 "" H 4500 5000 50  0001 C CNN
F 3 "" H 4500 5000 50  0001 C CNN
	1    4500 5000
	1    0    0    -1  
$EndComp
Connection ~ 4500 5000
Wire Wire Line
	4200 4600 4200 4450
$Comp
L power:VCC #PWR02
U 1 1 5C689CC3
P 3900 4450
F 0 "#PWR02" H 3900 4300 50  0001 C CNN
F 1 "VCC" H 3917 4623 50  0000 C CNN
F 2 "" H 3900 4450 50  0001 C CNN
F 3 "" H 3900 4450 50  0001 C CNN
	1    3900 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 4250 4500 4100
Wire Wire Line
	4500 3650 4500 3700
$Comp
L power:VCC #PWR01
U 1 1 5C68A187
P 4500 3650
F 0 "#PWR01" H 4500 3500 50  0001 C CNN
F 1 "VCC" H 4517 3823 50  0000 C CNN
F 2 "" H 4500 3650 50  0001 C CNN
F 3 "" H 4500 3650 50  0001 C CNN
	1    4500 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3900 4800 3700
Wire Wire Line
	4800 3700 4500 3700
$Comp
L Device:R R1
U 1 1 5C68A6B3
P 4500 3900
F 0 "R1" H 4570 3946 50  0000 L CNN
F 1 "1K" H 4570 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4430 3900 50  0001 C CNN
F 3 "~" H 4500 3900 50  0001 C CNN
	1    4500 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 4100 4500 4050
Connection ~ 4500 4100
Wire Wire Line
	4500 3750 4500 3700
Connection ~ 4500 3700
$Comp
L Device:R R2
U 1 1 5C68AFD0
P 4050 4450
F 0 "R2" V 3843 4450 50  0000 C CNN
F 1 "1K" V 3934 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3980 4450 50  0001 C CNN
F 3 "~" H 4050 4450 50  0001 C CNN
	1    4050 4450
	0    1    1    0   
$EndComp
Connection ~ 4200 4450
Wire Wire Line
	3900 4800 3550 4800
$Comp
L Device:D_ALT D1
U 1 1 5C68B454
P 4150 4100
F 0 "D1" H 4150 4316 50  0000 C CNN
F 1 "C64898" H 4150 4225 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123F" H 4150 4100 50  0001 C CNN
F 3 "~" H 4150 4100 50  0001 C CNN
	1    4150 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 4100 4300 4100
Wire Wire Line
	4000 4100 3550 4100
Wire Wire Line
	4800 4300 5550 4300
Text Label 3700 4100 0    50   ~ 0
A
Text Label 3700 4800 0    50   ~ 0
ENABLE
Text Label 5250 4300 0    50   ~ 0
~Q
$Comp
L linus:LOGO G1
U 1 1 5C68CCE4
P 5600 6600
F 0 "G1" H 5600 6290 60  0001 C CNN
F 1 "LOGO" H 5600 6910 60  0001 C CNN
F 2 "linus:linus_logo_negative" H 5600 6600 50  0001 C CNN
F 3 "" H 5600 6600 50  0001 C CNN
	1    5600 6600
	1    0    0    -1  
$EndComp
$EndSCHEMATC
