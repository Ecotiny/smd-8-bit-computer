boardPlacements = "merged.placement.txt"
topFootprints = "nand-top-pos.csv"
bottomFootprints = "nand-bottom-pos.csv"

outTopFootprints = "panel-top-pos.csv"
outBottomFootprints = "panel-bottom-pos.csv"
bomFilename = "nand-bom.csv"

#Quantity, Designator, Description, Distributor, Distributor P/N, Manufacturer, Manufacturer P/N
bomConfigs = {'"Q_NPN_BEC"' : "Transistors (NPN/PNP) SOT-23 RoHS,LCSC,C305434,Foshan Blue Rocket Elec,MMBT3904",
              #'"1K"' : "Chip Resistor - Surface Mount 1KOhms ±5% 1/16W 0402 RoHS,LCSC,C105637,YAGEO,RC0402JR-071KL" ,
              #'"4.7K"' : "Chip Resistor - Surface Mount 4.7KOhms ±5% 1/16W 0402 RoHS,LCSC,C258128,Guangdong Fenghua Advanced Tech, RC-02W472JT",
              '"4.7K"' : "Chip Resistor - Surface Mount 47KOhms ±5% 1/16W 0402 RoHS,LCSC,C25563,Uniroyal Elec,0402WGJ0473TCE",
              '"1K"' : "Chip Resistor - Surface Mount 47KOhms ±1% 1/16W 0402 RoHS,LCSC,C93943,YAGEO,RC0402FR-0747KL",
              '"DF37C-10DS-0-4V-51(MALE)"' : "Mezzanine Connectors (Board to Board) SMD RoHS,LCSC,C324719,Hirose,DF37C-10DP-0.4V(51)"
              }
boardPositions = []

with open(boardPlacements, "rt") as bp:
    for placestr in bp.readlines():
        places = placestr[:-1].split(" ")[1:] # remove trailing newline, and board name
        places[1] = float(places[1][:-1])
        places[0] = float(places[0])
        boardPositions.append(places)

print(boardPositions)

boardPositions = [[0,0]]
topPlacements = []

# Ref,Val,Package,PosX,PosY,Rot,Side
compNums = {}
# find out how many of each component type there are
with open(topFootprints, "rt") as tf:
    with open(bottomFootprints, "rt") as bf:
        lines = tf.readlines() + bf.readlines()
        for placestr in lines[1:]: # exclude title line
            placestr = placestr[:-1] # remove trailing newline
            ref, val, package, posX, posY, rot, side = placestr.split(",")
            ref = ref[1]
            try: 
                compNums[ref] += 1
            except KeyError:
                compNums[ref] = 1

def offset(infilename, outfilename, bomfilename):
    csvoutstr = ""
    bomoutstr = ""
    with open(infilename, "rt") as sfp: # single footprint positions
        lines = sfp.readlines()
        print(lines)
        csvoutstr += lines[0] # make sure the csv has the title line
    
        # component positions for each board
        for bpi in range(len(boardPositions)):
            bp = boardPositions[bpi]
            for placestr in lines[1:]:
                placestr = placestr[:-1]
                ref, val, package, posX, posY, rot, side = placestr.split(",")
                posX = round(float(posX) + bp[0], 5)
                posY = round((float(posY) * 1) + bp[1], 5)
                
                refNum = int(ref[2:-1])
                refNum = refNum + (compNums[ref[1]] * bpi) 
                ref = '"{}{}"'.format(ref[1], refNum)
                
                outstr = "{0},{1},{2},{3:.6f},{4:.6f},{5},{6}\n".format(ref, val, package, posX, posY, rot, side)
                csvoutstr += outstr

                # bom stuff
                bomout = "{0},{1},{2}\n".format(1, ref, bomConfigs[val])
                bomoutstr += bomout
    with open(outfilename, "xt") as of:
        of.write(csvoutstr)
    with open(bomfilename, "at") as bf:
        bf.write(bomoutstr)

offset(topFootprints, outTopFootprints, bomFilename)
offset(bottomFootprints, outBottomFootprints, bomFilename)

