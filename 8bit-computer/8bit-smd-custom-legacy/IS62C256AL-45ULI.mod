PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
SOIC127P1210X305-28N
$EndINDEX
$MODULE SOIC127P1210X305-28N
Po 0 0 0 15 00000000 00000000 ~~
Li SOIC127P1210X305-28N
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -1.56214 -10.4493 1.00137 1.00137 0 0.05 N V 21 "SOIC127P1210X305-28N"
T1 -1.32535 10.2676 1.00026 1.00026 0 0.05 N V 21 "VAL**"
DS -4.1 -9.1 4.1 -9.1 0.2 21
DS 4.1 -9.1 4.1 9.1 0.2 21
DS 4.1 9.1 -4.1 9.1 0.2 21
DS -4.1 9.1 -4.1 -9.1 0.2 21
DS -6.95 -9.6 6.95 -9.6 0.05 26
DS 6.95 -9.6 6.95 9.6 0.05 26
DS 6.95 9.6 -6.95 9.6 0.05 26
DS -6.95 9.6 -6.95 -9.6 0.05 26
DC -3 -8 -2.5 -8 0 24
DC -5.7 -9.2 -5.47639 -9.2 0 21
$PAD
Sh "1" R 2.25 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.55 -8.225
$EndPAD
$PAD
Sh "2" R 2.25 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.55 -6.955
$EndPAD
$PAD
Sh "3" R 2.25 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.55 -5.685
$EndPAD
$PAD
Sh "4" R 2.25 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.55 -4.415
$EndPAD
$PAD
Sh "5" R 2.25 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.55 -3.145
$EndPAD
$PAD
Sh "6" R 2.25 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.55 -1.875
$EndPAD
$PAD
Sh "7" R 2.25 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.55 -0.605
$EndPAD
$PAD
Sh "8" R 2.25 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.55 0.665
$EndPAD
$PAD
Sh "9" R 2.25 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.55 1.935
$EndPAD
$PAD
Sh "10" R 2.25 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.55 3.205
$EndPAD
$PAD
Sh "11" R 2.25 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.55 4.475
$EndPAD
$PAD
Sh "12" R 2.25 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.55 5.745
$EndPAD
$PAD
Sh "13" R 2.25 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.55 7.015
$EndPAD
$PAD
Sh "14" R 2.25 0.6 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.55 8.285
$EndPAD
$PAD
Sh "15" R 2.25 0.6 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.55 8.225
$EndPAD
$PAD
Sh "16" R 2.25 0.6 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.55 6.955
$EndPAD
$PAD
Sh "17" R 2.25 0.6 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.55 5.685
$EndPAD
$PAD
Sh "18" R 2.25 0.6 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.55 4.415
$EndPAD
$PAD
Sh "19" R 2.25 0.6 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.55 3.145
$EndPAD
$PAD
Sh "20" R 2.25 0.6 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.55 1.875
$EndPAD
$PAD
Sh "21" R 2.25 0.6 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.55 0.605
$EndPAD
$PAD
Sh "22" R 2.25 0.6 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.55 -0.665
$EndPAD
$PAD
Sh "23" R 2.25 0.6 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.55 -1.935
$EndPAD
$PAD
Sh "24" R 2.25 0.6 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.55 -3.205
$EndPAD
$PAD
Sh "25" R 2.25 0.6 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.55 -4.475
$EndPAD
$PAD
Sh "26" R 2.25 0.6 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.55 -5.745
$EndPAD
$PAD
Sh "27" R 2.25 0.6 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.55 -7.015
$EndPAD
$PAD
Sh "28" R 2.25 0.6 0 0 1800
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.55 -8.285
$EndPAD
$EndMODULE SOIC127P1210X305-28N
